var shortid = require('shortid');

var invoke = require('./utils').invoke;
var Emitter = require('./emitter');

function EmittersBus(id) {
  this.id = id || shortid.generate();
  this.emitters = [];
}

EmittersBus.prototype.createEmitter = function () {
  var emitters = this.emitters;
  
  var emitter = new Emitter(
    function () {
      for (var i = 0; i < emitters.length; i++) {
        if (emitters[i] !== emitter) {
          invoke(emitters[i].fireEvent, emitters[i], arguments);
        }
      }
    },
    this.id + ':' + this.emitters.length
  );
  
  emitters
  .push(emitter);
  
  return emitter;
};

module.exports = EmittersBus;