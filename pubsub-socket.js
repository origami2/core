function PubSubSocket(socket, token) {
  if (!socket) throw new Error('socket is required');
  this.token = token;
  
  this.socket = socket;
  var listeners = this.listeners = {};
  
  socket
  .on(
    'event',
    function (eventName, message, callback) 
    {
      if (!listeners[eventName]) return;
      
      for (var i = 0; i < listeners[eventName].length; i++) {
        listeners[eventName][i](message, callback);
      }
    }
  );
}

PubSubSocket.prototype.on = function (eventName, handler) {
  if (!this.listeners[eventName]) {
    this.socket.emit('subscribe', eventName, this.token);
    this.listeners[eventName] = [];
  }
  
  this.listeners[eventName].push(handler);
};

PubSubSocket.prototype.emit = function (eventName, message) {
  this.socket.emit('publish', eventName, this.token, message);
};

PubSubSocket.prototype.off = function (eventName, handler) {
  if (!this.listeners[eventName]) return;
  
  this.listeners[eventName].splice(this.listeners[eventName].indexOf(handler), 1);
};

module.exports = PubSubSocket;