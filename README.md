# Origami 2 core

> Origami is a platform for making simple to create *scalable*, *secure*, *extensible* applications.
> It stands on the idea of taking microkernel design concepts and push them to real world distributed application building.

## Components

### The stack

The stack is the core, the engine of an origami application. It is supposed to do as little as possible. But it does several things:

- Acts as an event bus
- Keeps track of connected plugins
- Runs security checks on method invocations
- Acts as a round-robin load balancer for plugins

It requires a private key to be provided. Private keys are assumed to be 512-byte minimum RSA PEM strings. You can generate RSA keys with the Crypto Utils

You probably would want not to use it directly but to use it with its [socket.io helper](https://gitlab.com/origami2/stack-io).

---

### The name registry

It keeps keeps track of authorized names/public keys and provides a challenge mechanism to identify an alien party that claims a name. Thus, it requires a private key.

--- 

### The crane

The crane component manages connections over a socket. It relies on two other componets: Penelope and Name registry, and requires three parameters to be instantiated:

- Name registry
- Incoming connetions handler
  
  is a function in the form of (socket, params): should use the socket according to the stablished parameters.

- Socket: a socket in which another crane is listening

Connections will succeed if:

- clamied namespace is public
- the crane name registry has the public key for the incoming connection claimed namespace

#### .createSocket(namespace, initializer, params)

Returns a promise. It will try to open a socket to the other end. If it succeds, the promise will resolve. The params will be passed to the other end.
Note that the params object .namespace property will be overwritten by the value provided in the params. If the key pair challenge verifies that you own the claimed namespace, the initializer function will be invoked with a subsocket and the params object.

---

### Penelope

Handles socket multiplexation. It requires a socket object, over which `opensubsocket` and `subsocket-message` events will be sent.

#### .openSubsocket

Returns: Promise object
Params:

- `kind`: a string identifying the kind of the incoming handler
- `params`: params to be passed to the other penelope on socket opening.

#### .addHandler

Params:

- `initializer`: the subsocket initializer function. It will receive a socket object that will map events to the open subsocket, the params object, and is expected to return a Promise. Once the promise is resolved the subsocket is considered to be initialized.
- `kind`: a string identifying the kind of the incoming handler
- `handler`: function to be invoked when an incomiing handler is required to be open from the other end when the kind specified matches. The function will receive two parameters: the subsocket object, and the open parameters specified from the other end. It is expected to return a Promise object. If promise is resolved, messages will pass, otherwise subsocket will be rejected.
  > Please note that both incoming and outgoing subsocket messages will be held until handler promise is resolved. This handler is ment to handle other requirements asynchronously.

---

### Plugins

Origami plugins publishes functions to other components. It requires a constructor function to be passed that helds the API methods. API methods are read from the provided function and published arguments will be taken from it's prototype function parameter names.

### constructor

```
function Calculator(startValue) {
  this.value = startValue || 0;
}

Calculator.prototype.sum = function (what) {
  this.value += what;
  
  return this.value;
}

var plugin = new Plugin(Calculator);
```

#### .createInstance

Returns: a Promise which resolves the new API instance or rejects with the creation error.

Params:

- `context`: object with the values to be passed to the constructor function.
  > it is mapped by argument names. For example `{ startValue: 400 }`

### .invokeMethod

Returns: a Promise which resolves the api method success result or an error ocurred during instance creation or method invocation.

Params:

- `context`: object with the values to be passed to the constructor function.
  > it is mapped by argument names. For example `{ startValue: 400 }`
- `methodName`: a string matching the method name. For example `"sum"`

- `methodParams`: object with the values to be passed to the method function inside the created instance.
  > it is mapped by argument names. For example `{ what: 100 }`

### .invokeMethodInInstance

Same as `.invokeMethod` but requires an instance parameter instead of a context.

### .describeMethods

Returns: the method list of the instance in the form of an object holding the method name and a string array list with the parameter names. For example: `{"sum": ["what"]}`

### .getName

Returns: the constructor function name. For example: `"Calculator"`

---

### Emitters bus

The emitter bus creates connected event emitter instances. If you emit an event in an instance it will be broadcasted to all others except itself.
Acts similar to socket.io socket instances, but different to node.js EventEmitter, which will raise the event on the same instance.

---

### Events

Plugins can subscribe to events. Events, unlike methods, do not wait for any result and do not care about errors. You will have to handle them by yourself.
An `emit` special parameters is injected into Plugin instances, which will raise events using the same context that the plugin was instantiated. For example, the context might be an user context.

`emit` function receive only one parameter: `message`, an that is the only parameter that will be forwarded to subscribers.

Plugins can subscribe to events. But please note that event names follow a tree-like subscription. For example, if you broadcast a `something.somethingelse` it will be forwarded to both `something` and `somethingelse` listeners.

---

### Token providers

Token providers are similar to plugins, except that the method invocation result will be opaque to the client. It will be encrypted solely to the calling stack.

Returned tokens can be used to create both API clients and token provider clients.

> You might want to read about [token provider socket.io helper](https://gitlab.com/origami2/token-provider-io) and [token provider client socket.io helper](https://gitlab.com/origami2/token-provider-client-io).

---

### Clients

Creates a proxy object to interact with a stack instance through a socket.

Returns: proxy object
Params:

- socket *required*: object through which to invoke the plugins

- apis *required*: object with a plugin/method names/parameter names tree. For example: `{ "Calculator": { "sum": ["what"] } }.

- token *optional*: token to provide context to the invoked plugin.

> You might want to read about the [client socket.io helper](https://gitlab.com/origami2/client-io)
