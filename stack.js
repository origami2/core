var debug = require('debug')('origami:Stack');
var crypto = require('./crypto-utils');
var vm = require('vm');
var EventEmitter = require('events').EventEmitter;

function Stack(privateKey) {
  EventEmitter.call(this);
  
  this.plugins = {};
  this.tokenProviders = {};
  this.subscribers = {};
  this.trustedSubscribers = {};
  this.guards = {};

  if (!privateKey) throw new Error('private key is required');

  this.privateKey = crypto.asPrivateKey(privateKey);
  
  debug('Stack created');
}

Stack.prototype.addLocalPlugin = function (plugin, locals, dependencies, events) {
  if (!plugin) throw new Error('plugin is required');
  
  var PenelopePlugin = require('./penelope-plugin');
  
  var peneloperPlugin = new PenelopePlugin(plugin, locals, dependencies, events);
  
  var EmittersBus = require('./emitters-bus');
  var bus = new EmittersBus();
  
  var socket1 = bus.createEmitter();
  var socket2 = bus.createEmitter();
  
  var pluginName = plugin.getName();
  var pluginMethods = plugin.describeMethods();
  
  this.addPlugin(pluginName, socket1, pluginMethods);
  events = events || {};
  
  for (var eventName in events) {
    this.addTrustedSubscriber(socket1, eventName);
  }
  
  return peneloperPlugin(socket2, {});
};

Stack.prototype.getContextFromToken = function (token) {
  return  crypto.decryptAndVerify(
    token,
    this.privateKey,
    this.privateKey
  );
};

Stack.prototype.getPublicKey = function () {
  return this.privateKey.exportKey('pkcs1-public-pem');
};

Stack.prototype.getPrivateKey = function () {
  return this.privateKey.exportKey('pkcs1-private-pem');
};

Stack.prototype.listKnownEvents = function () {
  var knownEvents = [];
  
  for (var eventName in this.subscribers) {
    knownEvents.push(eventName);
  }
  
  return knownEvents;
};

Stack.prototype.listSubscribers = function (namespace) {
  var parts = namespace.split('.');
  var final = [];

  for (var i = 0; i < parts.length + 1; i++) {
    var candidate = parts.slice(0, i).join('.');

    final.push.apply(final, this.subscribers[candidate] || []);
  }

  return final;
};

Stack.prototype.listTrustedSubscribers = function (namespace) {
  var parts = namespace.split('.');
  var final = [];

  for (var i = 0; i < parts.length + 1; i++) {
    var candidate = parts.slice(0, i).join('.');

    final.push.apply(final, this.trustedSubscribers[candidate] || []);
  }

  return final;
};

Stack.prototype.addSubscriber = function (socket, namespace) {
  debug('.addSubscriber for %s', namespace);
  
  var subscribers = this.subscribers[namespace] = this.subscribers[namespace] || [];

  if (subscribers.indexOf(socket) !== -1) return;
  subscribers.push(socket);
  
  socket.on('disconnect', function () {
    subscribers.splice(subscribers.indexOf(socket), 1);
  });

  return Promise.resolve();
};

Stack.prototype.addTrustedSubscriber = function (socket, namespace) {
  debug('.addTrustedSubscriber for %s', namespace);
  
  var subscribers = this.trustedSubscribers[namespace] = this.trustedSubscribers[namespace] || [];

  if (subscribers.indexOf(socket) !== -1) return;
  subscribers.push(socket);
  
  socket.on('disconnect', function () {
    subscribers.splice(subscribers.indexOf(socket), 1);
  });
};

Stack.prototype.removeSubscriber = function (socket, namespace) {
  debug('.removeSubscriber for %s', namespace);
  
  var subscribers = this.subscribers[namespace] = this.subscribers[namespace] || [];

  subscribers.splice(subscribers.indexOf(socket), 1);

  return Promise.resolve();
};

Stack.prototype.publishEvent = function (eventName, token, message, socketFilter) {
  debug('.publishEvent %s', eventName);
  
  socketFilter = socketFilter || function () { return true; };
  var subscribers = this.listSubscribers(eventName).filter(socketFilter);
  
  for (var i = 0; i < subscribers.length; i++) {
    subscribers[i].emit('event', eventName, message);
  }
  
  var trudtedSubscribers = this.listTrustedSubscribers(eventName).filter(socketFilter);
  
  var context = token ? this.getContextFromToken(token) : {};

  for (var i = 0; i < trudtedSubscribers.length; i++) {
    trudtedSubscribers[i].emit('event', eventName, token, context, message);
  }
};

Stack.prototype.addPlugin = function (pluginName, socket, methods) {
  debug('.addPlugin %s', pluginName);
  
  var self = this;
  
  var emitApisChanged = function () {
    var apis = self.listPlugins();
    
    socket.emit('apis-changed', apis);
    self.publishEvent('apis-changed', null, apis);
  };
  
  this.on('plugin-added', emitApisChanged);
  this.on('plugin-removed', emitApisChanged);
  
  socket
  .on('disconnect', function () {
    self.removeListener('plugin-added', emitApisChanged);
    self.removeListener('plugin-removed', emitApisChanged);
  
    self.plugins[pluginName].sockets.splice(self.plugins[pluginName].sockets.indexOf(socket), 1);

    if (self.plugins[pluginName].sockets.length === 0) {
      delete self.plugins[pluginName];
    }
  });
  
  socket
  .on(
    'describe-apis', 
    function (callback) {
      var apis = self.listPlugins();
      
      callback(null, apis);
    }
  );
  
  socket
  .on(
    'api', 
    function (stackToken, pluginName, methodName, methodParams, callback) {
      self
      .simpleInvokeMethod(
        stackToken,
        pluginName,
        methodName,
        methodParams
      )
      .then(
        function (result) {
          callback(null, result);
        }
      )
      .catch(
        function (err){
          callback(err);
        }
      );
    }
  );
  
  socket
  .on(
    'publish',
    function (eventName, token, message) {
      self
      .publishEvent(eventName, token, message);
    }
  );

  this.plugins[pluginName] = this.plugins[pluginName] || {
    sockets: [],
    methods: methods
  };

  this.plugins[pluginName].sockets.push(socket);
  
  debug('.addPlugin %s done', pluginName);
  
  this.emit('plugin-added', pluginName, methods);
};

Stack.prototype.listenSocket = function (socket) {
  var self = this;

  self.addClient(socket);
};

Stack.prototype.addGuard = function (guardName, socket) {
  var self = this;

  this.guards[guardName].socket = socket;

  socket
  .on('add-check', function (signed, callback) {
    var decrypted;

    try {
      decrypted = crypto
      .decryptAndVerify(
        signed,
        self.getPrivateKey(),
        self.getGuardPublicKey(guardName)
      );
    } catch (e) {
      return callback('invalid signature');
    }

    self.addGuardCheck(guardName, decrypted.name, decrypted.code);

    callback();
  });

  socket.on('disconnect', function () {
    self.guards[guardName].socket = null;
  });
};

Stack.prototype.addGuardCheck = function (guardName, checkName, code) {
  this.guards[guardName].checks[checkName] = code;
};

Stack.prototype.invokeMethod = function (token, context, pluginName, methodName, params) {
  debug('invoke %s.%s', pluginName, methodName);
  
  var self = this;

  if (!this.plugins[pluginName]) return Promise.reject('invalid plugin');
  if (!this.plugins[pluginName].methods[methodName]) return Promise.reject('invalid method');

  return new Promise(function (resolve, reject) {
    var checkContext = JSON.parse(JSON.stringify(context));
    checkContext.plugin = pluginName;
    checkContext.method = methodName;

    for (var guardName in self.guards) {
      for (var checkName in self.guards[guardName].checks) {
        var authorized = self.runCheck(
          guardName,
          checkName,
          checkContext
        );

        if (authorized) continue;
        return reject('not authorized');
      }
    }

    var pluginSockets = self.plugins[pluginName].sockets;
    
    pluginSockets.push(pluginSockets.splice(0,1)[0]); // rotate

    var pluginSocket = pluginSockets[0];

    pluginSocket
    .emit(
      'api',
      token,
      context,
      methodName,
      params,
      function (err, result) {
        if (err) {
          debug('%s.%s error:', pluginName, methodName, err);
          return reject(err);
        }

        debug('%s.%s success:', pluginName, methodName, result);
        resolve(result);
      }
    );
  });
};

Stack.prototype.runCheck = function (guardName, checkName, context) {
  var code = this.guards[guardName].checks[checkName];

  var sandbox = {};

  context = JSON.parse(JSON.stringify(context));

  for (var k in context) sandbox[k] = context[k];

  var authorized = true;

  sandbox.reject = function () { authorized = false; };

  vm.runInNewContext(
    '(function () {' + code + '})()',
    sandbox
  );

  return authorized;
};

Stack.prototype.listGuardChecks = function () {
  var checks = {};

  for (var k in this.guards) {
    checks[k] = Object.keys(this.guards[k].checks);
  }

  return checks;
};

Stack.prototype.listPlugins = function () {
  var result = {};

  for (var n in this.plugins) {
    result[n] = this.plugins[n].methods;
  }

  return result;
};

Stack.prototype.authorizeGuardPublicKey = function (guardName, publicKey) {
  if (!guardName) throw new Error('guard name is required');

  try {
    var guardPublicKey = crypto.asPublicKey(publicKey);

    this.guards[guardName] = this.guards[guardName] || {
      publicKey: guardPublicKey,
      socket: null,
      checks: {}
    };
  } catch (e) {
    throw new Error('invalid key');
  }
};

Stack.prototype.getGuardPublicKey = function (guardName) {
  return this.guards[guardName].publicKey.exportKey('pkcs1-public-pem');
};

Stack.prototype.isGuardConnected = function (guardName) {
  if (!this.guards[guardName]) return false;

  if (this.guards[guardName].socket) return true;

  return false;
};

Stack.prototype.listGuards = function () {
  return Object.keys(this.guards);
};

Stack.prototype.removeSocket = function (socket) {
  socket.removeAllListeners('disconnect');
  socket.removeAllListeners('describe-api');
  socket.removeAllListeners('describe-apis');
  socket.removeAllListeners('describe-token-providers');
  socket.removeAllListeners('api');
  socket.removeAllListeners('subscribe');
  socket.removeAllListeners('publish');
  socket.removeAllListeners('get-token');
};

Stack.prototype.addTokenProvider = function (tokenProviderName, socket, methods, publicKey) {
  if (!socket) throw new Error('socket is required');
  if (!tokenProviderName) throw new Error('token provider name is required');
  if (!methods) throw new Error('methods are required');
  if (!publicKey) throw new Error('public key is required');
  
  debug('.addTokenProvider %s', tokenProviderName);

  var tokenProviderKey = crypto.asPublicKey(publicKey);
  
  this.tokenProviders[tokenProviderName] = {
    socket: socket,
    methods: methods,
    publicKey: tokenProviderKey
  };
  
  debug('.addTokenProvider %s done', tokenProviderName);
};

Stack.prototype.verifyToken = function (tokenProviderName, token) {
  return crypto.decryptAndVerify(
    token,
    this.getPrivateKey(),
    this.tokenProviders[tokenProviderName].publicKey
  );
};

Stack.prototype.listTokenProviders = function () {
  var result = {};

  for (var n in this.tokenProviders) {
    result[n] = this.tokenProviders[n].methods;
  }

  return result;
};

Stack.prototype.addClient = function (socket) {
  if (!socket) throw new Error('socket is required');
  var self = this;
  
  socket
  .on(
    'describe-api',
    function (pluginName, callback) {
      if (!self.plugins[pluginName]) return callback('Plugin unavailable');

      callback(null, self.plugins[pluginName].methods);
    }
  );

  socket
  .on(
    'describe-apis',
    function (callback) {
      callback(null, self.listPlugins());
    }
  );

  socket
  .on(
    'describe-token-providers',
    function (callback) {
      var result = {};
      for (var tokenProviderName in self.tokenProviders) {
        result[tokenProviderName] = {
          methods: self.tokenProviders[tokenProviderName].methods,
          publicKey: self.tokenProviders[tokenProviderName].publicKey.exportKey('pkcs1-public-pem')
        };
      }

      callback(null, result);
    }
  );

  socket
  .on(
    'get-token',
    function (
      stackToken,
      tokenProviderName,
      methodName,
      params,
      callback
    ) {
      self
      .getToken(stackToken, tokenProviderName, methodName, params)
      .then(function (result) {
        callback(null, result);
      })
      .catch(callback);
    }
  );

  socket
  .on(
    'api',
    function (
      stackToken,
      pluginName,
      methodName,
      methodParams,
      callback
    ) {
      self
      .simpleInvokeMethod(stackToken, pluginName, methodName, methodParams)
      .then(function (result) {
        callback(null, result);
      })
      .catch(function (err) {
        callback(err);
      });
    }
  );
};

Stack.prototype.simpleInvokeMethod = function (token, pluginName, methodName, methodParams) {
  var context = {};
  var self = this;

  if (token) {
    context = crypto.decryptAndVerify(
      token,
      self.getPrivateKey(),
      self.getPublicKey()
    );
  }

  return self
          .invokeMethod(
            token,
            context,
            pluginName,
            methodName,
            methodParams
          );
};

Stack.prototype.getToken = function (
  stackToken,
  tokenProviderName,
  methodName,
  params
) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var context = stackToken ? self.getContextFromToken(stackToken)
                             : {};

    var tokenProviderSocket = self.tokenProviders[tokenProviderName].socket;


    tokenProviderSocket
    .emit(
      'get-token',
      stackToken,
      context,
      methodName,
      params,
      function (err, result) {
        if (err) return reject(err);

        if (result) {
          var newToken = crypto.encryptAndSign(
            result,
            self.privateKey,
            self.privateKey
          );

          return resolve(newToken);
        }

        reject('no token');
      }
    );
  });
};

require('util').inherits(Stack, EventEmitter);

module.exports = Stack;
