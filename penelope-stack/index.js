
var TokenProvider = require('./token-provider');
var Plugin = require('./plugin');
var Client = require('./client');
var PubSub = require('./pubsub');

function PenelopeStack(stack) {
  if (!stack) throw new Error('stack is required');
  
  return function (socket, params) {
    if (!socket) throw new Error('socket is required');
    if (!params) throw new Error('params are required');
    
    if (params.namespace.indexOf('Plugin.') === 0) {
      return (new Plugin(stack))(socket, params);
    } else if (params.namespace.indexOf('TokenProvider.') === 0) {
      return (new TokenProvider(stack))(socket, params);
    } else if (params.namespace === 'Client') {
      return (new Client(stack))(socket, params);
    } else if (params.namespace === 'Events') {
      return (new PubSub(stack))(socket, params);
    }
  };
}

PenelopeStack.Plugin = Plugin;
PenelopeStack.Client = Client;
PenelopeStack.PubSub = PubSub;

module.exports = PenelopeStack;