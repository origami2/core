function PenelopeStackPubSub(stack) {
  if (!stack) throw new Error('stack is required');

  return function (socket, params) {
    var subscribed = [];

    if (!socket) throw new Error('socket is required');

    socket
      .on('subscribe', function (namespace, callback) {
        stack.addSubscriber(socket, namespace).then(() => {
          if (subscribed.indexOf(namespace) !== -1) return;

          subscribed.push(namespace);

          if (callback) callback();
        }, (err) => callback(err));
      });

    socket
      .on('disconnect', function () {
        Promise.all(
          subscribed.map((namespace) => stack.removeSubscriber(socket, namespace).then(() => subscribed.splice(subscribed.indexOf(namespace), 1)))
        )
          .then(() => {
            s = subscribed;
          });
      });

    socket
      .on('unsubscribe', function (namespace, callback) {
        if (subscribed.indexOf(namespace) === -1) return;

        stack.removeSubscriber(socket, namespace)
          .then(() => {
            subscribed.splice(subscribed.indexOf(namespace), 1);

            if (callback) callback();
          }, (err) => callback(err));
      });

    socket
      .on('publish', function (namespace, token, message) {
        stack
          .publishEvent(
            namespace,
            token,
            message,
            function (filteringSocket) {
              return filteringSocket !== socket;
            }
          );
      });

    return Promise.resolve();
  };
}

module.exports = PenelopeStackPubSub;