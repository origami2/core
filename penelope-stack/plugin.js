function PenelopeStackPlugin(stack) {
  if (!stack) throw new Error('stack is required');
  
  return function (socket, params) {
    if (!socket) throw new Error('socket is required');
    if (!params) throw new Error('params are required');
    if (!params.name) throw new Error('name is required');
    if (!params.methods) throw new Error('methods are required');
    if (!params.events) throw new Error('events are required');
    
    stack
    .addPlugin(
      params.name, 
      socket, 
      params.methods
    );
    
    for (var i = 0; i < params.events.length; i++) {
      var eventName = params.events[i];
      
      stack
      .addTrustedSubscriber(socket, eventName);
    }
    
    return Promise.resolve();
  };
}

module.exports = PenelopeStackPlugin;