function PenelopeStackClient(stack) {
  if (!stack) throw new Error('stack is required');

  return function (socket, params) {
    if (!socket) throw new Error('socket is required');

    var emitApisChanged = function () {
      socket.emit('apis-changed', stack.listPlugins());
    };

    stack.on('plugin-added', emitApisChanged);
    stack.on('plugin-removed', emitApisChanged);

    socket
      .on('disconnect', function () {
        stack.removeListener('plugin-added', emitApisChanged);
        stack.removeListener('plugin-removed', emitApisChanged);
      });

    socket
      .on(
        'describe-apis',
        function (callback) {
          try {
            var apis = stack.listPlugins();

            callback(null, apis);
          } catch (e) {
            callback(e);
          }
        }
      );

    socket
      .on(
        'describe-token-providers',
        function (callback) {
          try {
            var apis = stack.listTokenProviders();

            callback(null, apis);
          } catch (e) {
            callback(e);
          }
        }
      );

    socket
      .on(
        'unsubscribe',
        function (eventName) {
          stack.removeSubscriber(socket, eventName);
        }
      );

    socket
      .on(
        'publish',
        function (eventName, token, message) {
          stack.publishEvent(eventName, token, message, function (s) {
            return s !== socket;
          });
        }
      );

    socket
      .on(
        'subscribe',
        function (eventName) {
          stack.addSubscriber(socket, eventName);
        }
      );

    socket
      .on(
        'api',
        function (token, pluginName, methodName, methodParams, callback) {
          stack
            .simpleInvokeMethod(token, pluginName, methodName, methodParams)
            .then(function (result) {
              callback(null, result);
            })
            .catch(function (err) {
              callback(err);
            });
        }
      );

    socket
      .on(
        'get-token',
        function (token, tokenProviderName, methodName, methodParams, callback) {
          stack
            .getToken(token, tokenProviderName, methodName, methodParams)
            .then(function (result) {
              callback(null, result);
            })
            .catch(function (err) {
              callback(err);
            });
        }
      );

    return Promise.resolve();
  };
}

module.exports = PenelopeStackClient;