var jsHelpers = require('./js-function-helpers');

function TokenProviderClient (socket, apis, token) {
  if (!socket) throw new Error('socket is required');
  if (!apis) throw new Error('apis are required');

  var api = {};

  for (var apiName in apis) {
    var ShadowClient = jsHelpers.createApi(
      apiName,
      apis[apiName],
      function (apiName, methodName, argNames) {
        return jsHelpers.createMethod(
          methodName,
          argNames,
          function () {
            var argsObject = {};

            for (var i = 0; i < arguments.length; i++) {
              argsObject[argNames[i]] = arguments[i];
            }

            return new Promise(function (resolve, reject) {
              socket
              .emit(
                'get-token',
                token,
                apiName,
                methodName,
                argsObject,
                function (err, result) {
                  if (err) return reject(err);

                  resolve(result);
                }
              );
            });
          }
        );
      }
    );

    api[apiName] = Object.create(ShadowClient.prototype);
    ShadowClient.apply(api[apiName]);
  }
  
  return api;
}

module.exports = TokenProviderClient;
