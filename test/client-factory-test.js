var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var ClientFactory = require('../client-factory');

describe('ClientFactory', function () {
  it('exports a function', function () {
    assert.equal(typeof(ClientFactory), 'function');
  });
  
  describe('.createClient', function () {
    var socket;
    var factory;
    
    beforeEach(function () {
      socket = new EventEmitter();
      factory = new ClientFactory();
    });

    it('requires a socket', function () {
      assert
      .throws(
        function () {
          factory.createClient();
        },
        /socket is required/
      );
    });
    
    
    it('emits describe-apis', function (done) {
      socket
      .on('describe-apis', function (callback) {
        try {
          assert(typeof(callback), 'function');
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      factory.createClient(socket);
    });

    it('returns a promise', function () {
      socket
      .on('describe-apis', function (callback) {
        callback(null, { 'api1': { 'method1': [ 'param1' ] } });
      });
      
      assert(factory.createClient(socket) instanceof Promise);
    });
    
    it('resolves on describe-apis callback', function (done) {
      socket
      .on('describe-apis', function (callback) {
        callback(null, { 'api1': { 'method1': [ 'param1' ] } });
      });
      
      factory
      .createClient(socket)
      .then(function (client) {
        try {
          assert(client);
          assert(client.api1);
          assert.equal(typeof(client.api1.method1), 'function');
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('resolves on describe-apis error callback', function (done) {
      socket
      .on('describe-apis', function (callback) {
        callback('the error');
      });
      
      factory
      .createClient(socket)
      .catch(function (err) {
        try {
          assert.equal(err, 'the error');
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('client is updated after apis-changed', function (done) {
      socket
      .on('describe-apis', function (callback) {
        callback(null, {});
      });
      
      factory.createClient(socket)
      .then(function (client) {
        try {
          assert(client);
          assert(!client.api1);
          
          socket.emit('apis-changed', { 'api1': { 'method1': [ 'param1' ] } });
          
          assert(client.api1);
          assert.equal(typeof(client.api1.method1), 'function');
          
          done();
        } catch (e) {
          done(e);
        }
      })
    });
  });
})