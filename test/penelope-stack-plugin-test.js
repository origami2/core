var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var PenelopeStackPlugin = require('../penelope-stack/plugin');

describe('PenelopeStackPlugin', function () {
  var fakeStack, fakeSocket;

  beforeEach(function () {
    fakeStack = new EventEmitter();
    fakeSocket = new EventEmitter();
  });

  it('exports a function', function () {
    assert.equal(typeof (PenelopeStackPlugin), 'function');
  });

  it('requires a stack', function () {
    assert.throws(
      function () {
        new PenelopeStackPlugin();
      },
      /stack is required/
    );
  });

  it('requires a socket', function () {
    var target = new PenelopeStackPlugin(fakeStack);

    assert.throws(
      function () {
        target();
      },
      /socket is required/
    );
  });

  it('requires params', function () {
    var target = new PenelopeStackPlugin(fakeStack);

    assert.throws(
      function () {
        target(fakeSocket);
      },
      /params are required/
    );
  });

  it('requires name param', function () {
    var target = new PenelopeStackPlugin(fakeStack);

    assert.throws(
      function () {
        target(fakeSocket, {});
      },
      /name is required/
    );
  });

  it('requires methods param', function () {
    var target = new PenelopeStackPlugin(fakeStack);

    assert.throws(
      function () {
        target(fakeSocket, { name: {} });
      },
      /methods are required/
    );
  });

  it('requires events param', function () {
    var target = new PenelopeStackPlugin(fakeStack);

    assert.throws(
      function () {
        target(fakeSocket, { name: 'fake', methods: {} });
      },
      /events are required/
    );
  });

  it('adds events as trusted subscriber', function (done) {
    fakeStack.addPlugin = () => { };

    fakeStack.addTrustedSubscriber = (socket, eventName) => {
      try {
        assert.strictEqual(fakeSocket, socket);
        assert.equal(eventName, 'event1');

        done();
      } catch (e) {
        done(e);
      }
    };

    var target = new PenelopeStackPlugin(fakeStack);

    target(fakeSocket, { name: 'fake', methods: {}, events: ['event1'] });
  });

  it('returns a promise', function (done) {
    var target = new PenelopeStackPlugin(fakeStack);

    var fakeSocket = new EventEmitter();

    fakeStack.addPlugin = (pluginName, socket, methods) => {
      try {
        assert.equal(pluginName, 'Plugin1');
        assert.equal(socket, fakeSocket);
        assert.deepEqual(methods, { 'method1': ['p1'] });

        done();
      } catch (e) {
        done(e);
      }
    };

    var r = target(fakeSocket, { 'name': 'Plugin1', 'methods': { 'method1': ['p1'] }, 'events': [] });
    assert(r instanceof Promise);
  });
});