var testData = {
  "pair1": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPC\nmR31TJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS6\n7C08YAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF\n9ARu5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGVi\nPlhtpImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUI\nNoYL8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQABAoIBAQCFX9Mqbc/jDLGb\n92+Q2eqTtMEiafE7bmznX5voa+bVk863h8eRGWj2FnG8aL2Cfq0EZRb9BKxCrJtQ\nL8fibtmaoLSYfxA4BOicQKnSozEWOhU47mBtiLQd9cDHf852l/VDsSzDfcm6jj9+\nF0UGB1zzmEnB/Fkue0HOzNKBGmbGE1/RWo4+co3XYpq/YZdIQQadsV6G2ma1IApm\n5t9AmEzhjMJsVFTZqzWQpoHmJRJjfwqqugmt4gpBCuYMkVcuN0g1olOLobF+pITn\nNjVaYimiFDsjhZyM4c0MFUYB27TeG7RV+QigHM/DW9rTbvTXxnTl/O4mipUe4FsK\nTdsbu4kBAoGBAPwJeudZz0cYM7MPzYE0/ksA9OrJwbhg+BjUJQ7CtFijJp8Cxt3d\ncu6DC5GOke0dXe845DLwriYKqJbae0FIeQvHrFTRpC2gGI5CJ3nPrZ3jbJhQLnQA\nMoF7EbJdpMM0I1xZsZ1cOy7jDbQ6/5Za4RC59xyqZ8hIi8vn9JVAkurPAoGBAIrd\nHyQ0NuZsQCxqNKPvU+1QnpFaoy65yA1kLZ/JDghjTiplxmv0WMuf1B3rq09RvTKK\nCEc7S8CredzLDtgQF7KiENnMno9e8bcMAddC+UaKsV9Eoy4mXGe8vTnQsGvlMkZI\n27gkM1o+Sbsl45Ayr3eYcpJD6K+txSjJYO8B3GIzAoGAUQ5ndYIftHinH95kNDqr\n0clj+yKZ58df4vRPWrjpsVv/LsKA3Je8v9JrZQuaCM0aCbadRXi8OUXSRHnNjAhX\nzZ8Q4FJv37COVSoXcgiFiLK8mRuoZOwvUg8XeOq+83yQJsI96iLgccrZ/G3BB0UA\n/xUf0RtIt1QFibV2po2W8mcCgYBjLOXO35POIcX7cqbB5m3Ucd2uBkPBXWIpXkDP\ne7KP/wyWbzW1aD/6vd2quOQStFghvj+HUCwcINvZ+xRQ771dES5jvyYHU1Hi36p4\n6RZLcUaYudapYTBhzoR+xDMb/AdZ9zMlYoVikFXsWXUbSXfUPIanO+T1g2/qX1jh\nmjyhzQKBgQCvQdWSYMXaOnQRo/BHmRdR3X3aEZyLEn5UmXjxohUeB/Jtzb15FD/r\ny5ljkTVQyzYnRgwBuZBnUl/sCbxRp1CXbDmotHZ3DBwRJuP7KaPoj7dH+xfU6P3N\nuAm6CP+73AONmouW/8tVW60n2IkSH5I1ROYtPLrI6475M/hB4Anj+Q==\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPCmR31\nTJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS67C08\nYAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF9ARu\n5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGViPlht\npImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUINoYL\n8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQAB\n-----END RSA PUBLIC KEY-----"
  },
  "pair2": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4\nPKtgrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQae\nsmwcu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIi\nGaR/A/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTE\nS+NTkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylh\nX2+xsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQABAoIBAQCzPkfPKGiXj/ho\npviEbfmfR9W2w0F+1V03Wk1Rjo8whxH6Rg9DuJpJcDOza95W2B+uz0Ds64Z6av0j\nmiK8SU4T8S8Yl8merXzHKCdq0IUBzRRan2s/48dsaRhn+G+wSs5yvQWAz0eXHGhR\n2xaPvXSTZdqnPzF/mrgUC3mp538a3TuqNzJYfowY4d+Rn6+8K5EqcWHHBBINBp90\nhWPxGMp2mTFDsycQaQjDsfGJDAlFtQJ0FlrU0KAHt5KhaX7jMvOvD7AuL8aEnnWy\n9yTwb+POpPxrz1a+6p4U7YIPef3yVmWS+QUI2aDtgz7LMlxyTfkX4OiwmBGw51ED\niIwKCYwBAoGBAOfD1WkLhjqAqPDYdm1VkRYQILDWv6vUhznevw/J8yItnpYakRe7\njmhdiQh/p3ieOaA/l31p2ZR4OGT2hZwefAFYbwVoCOAtoIelbJwz5Mu1H7mMBuav\n23/K41edQ25zRbEjy1f20CMBHbEhF+64ufRGUUnJcA0gV9dO4ZvdvQ0jAoGBAMju\nwM/T0sTM83l1IPRvd7moTKzhOt2dJXq660PVY1FqLKSvXicCsFQtEd4J2K2Aik+n\n7A9m0DJx/QauPZ3Dk2K1dWCXqTRp7Yjhvpmw2mvrTfRNFww+Ths15XDCAtRWzUSV\n8yB9TnTH8opeUejWcoI8VikhVtkR8hF5m0xhGXEZAoGAYe4Rwu0nrpimyf3tLI23\nXIc8CPz0yHppGT7RpK5EmfCEzhAztr99kQPU305xSToyR1AXhtqvIVkbGy4/jpQi\n+b6QSsyG05Lz/d0cY3RZ/OlvmktcryUnrnvgTCkbURRMImlphqW3lNLn5OyC3FAZ\n4unDd1YyjplYBJZEJkQvmdkCgYBbtV0bfjq8yC54SRV5e0bR3hbg8QvFCmyIz7eB\nhmuGRUeLAt+ePoPsZ4W4KhZTWk/Ge4YqoWp1G8G4wt4sm48xhlDEfXQlEBPyXdZ4\nn1eR2hwCXQ0f8XAEy1ylUmeoMtYNb8NAGPEuK/RvYw1PkFFDT/ajXywcdyxc/Xv8\nKHVMQQKBgFPnGNRfLtCVfONwcveqfUeR4xS4UJkTNjLExTzBRKoUuKLbZwV5tHEr\nBFoMaM3V5pPLKfImO4BYZeuPdB1DBG3dsxL5G5TzWBauJq7OGAQmXuHecV4pblHm\nPO9yEktLnn8QAkbiePv+PhFFnSAa93cYHNtM68bBTdLrohBzZbDW\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4PKtg\nrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQaesmwc\nu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIiGaR/\nA/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTES+NT\nkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylhX2+x\nsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQAB\n-----END RSA PUBLIC KEY-----"
  }
};
var assert = require('assert');
var RSASocket = require('../rsa-socket');
var crypto = require('../crypto-utils');
var EventEmitter = require('events').EventEmitter;
var EmittersBus = require('../emitters-bus');

describe('RSA Socket', function () {
  this.timeout(400);
  
  var bus;
  var socket1;
  var socket2;
  
  beforeEach(function () {
    bus = new EmittersBus();
    socket1 = bus.createEmitter();
    socket2 = bus.createEmitter();
  });

  it('requires a private key to instantiate', function () {
    assert
    .throws(
      function () {
        new RSASocket();
      },
      /private key is required/
    );
  });

  it('rejects invalid private key', function () {
    assert
    .throws(
      function () {
        new RSASocket(
          'invalid private key'
        );
      },
      /invalid private key/
    );
  });

  it('accepts valid private key', function () {
    new RSASocket(
      testData.pair1.privateKey
    );
  });

  describe('.connect', function () {
    it('requires a socket', function () {
      var target = new RSASocket(testData.pair1.privateKey);

      assert
      .throws(
        function () {
          target.connect();
        },
        /socket is required/
      );
    });

    it('returns a Promise', function () {
      var target = new RSASocket(testData.pair1.privateKey);

      var fakeSocket = new EventEmitter();

      var promise = target.connect(fakeSocket);

      assert(promise instanceof Promise);
    });

    describe('rsa-hello', function () {
      it('emits rsa-hello', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          try {
            assert.equal(testData.pair1.publicKey, publicKey);
            assert(callback);

            done();
          } catch (e) {
            done(e);
          }
        });

        target.connect(fakeSocket);
      });

      it('accepts other side publicKey', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          done();
        });
      });
    });

    describe('rsa-event', function () {
      it('encrypts one side events', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          // YOLO: we don't need the real listener processing the event here
          fakeSocket.removeAllListeners('rsa-event');

          fakeSocket
          .on('rsa-event', function (args) {
            var decrypted = crypto
              .decryptAndVerify(
                args,
                testData.pair2.privateKey,
                testData.pair1.publicKey
              );
  
            assert(decrypted);
            assert.deepEqual(['my-event', 'my-arg'], decrypted);
  
            done();
          });

          securedSocket
          .emit('my-event', 'my-arg');
        });
      });

      it('handles callback if present', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });
        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          // YOLO: we don't really want to process the real event
          fakeSocket.removeAllListeners('rsa-event');

          fakeSocket
          .on(
            'rsa-event',
            function (args, callback) {
              var encrypted = crypto.encryptAndSign(
                ['my-result'],
                testData.pair2.privateKey,
                testData.pair1.publicKey
              );

              callback(encrypted);
            }
          );

          securedSocket
          .emit(
            'my-event',
            'my-arg',
            function () {
              done();
            }
          );
        });
      });

      it('receives other side events', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        var encrypted = crypto.encryptAndSign(
          ['my-other-event', 'my-other-arg'],
          testData.pair2.privateKey,
          testData.pair1.publicKey
        );

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .on(
            'my-other-event',
            function (arg1) {
              assert.equal('my-other-arg', arg1);
              done();
            }
          );

          fakeSocket.emit('rsa-event', encrypted);
        });
      });

      it('decrypts other side event callback result', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        var encryptedEvent = crypto.encryptAndSign(
          ['my-other-event'],
          testData.pair2.privateKey,
          testData.pair1.publicKey
        );

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .on(
            'my-other-event',
            function (callback) {
              try {
                callback('arg1', 'arg2');
              } catch (e) {
                done(e);
              }
            }
          );

          fakeSocket
          .emit(
            'rsa-event',
            encryptedEvent,
            function (result) {
              try {
                var decrypted = crypto.decryptAndVerify(
                  result,
                  testData.pair2.privateKey,
                  testData.pair1.publicKey
                );

                assert.deepEqual(
                  ['arg1', 'arg2'],
                  decrypted
                );

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });
      });
      
      it('emits event inside an emit callback', function (done) {
        var rsa1 = new RSASocket(testData.pair1.privateKey);
        var rsa2 = new RSASocket(testData.pair2.privateKey);
        
        Promise.all([
          rsa1.connect(socket1),
          rsa2.connect(socket2)
        ])
        .then(function (secureSockets) {
          var ss1 = secureSockets[0];
          var ss2 = secureSockets[1];
          
          ss2.on('e1', function (cb) {
            cb();
          });
          ss2.on('e2', function () {
            done();
          });
          ss1.emit('e1', function () {
            ss1.emit('e2');
          });
        });
      });
    });
    
    describe('#disconnect', function () {
      it('propagates disconnect', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .on(
            'disconnect',
            function () {
              done();
            }
          );

          fakeSocket.emit('disconnect');
        });
      });
    });
  });
});