var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var PenelopeStackClient = require('../penelope-stack/client');

describe('PenelopeStackClient', function () {
  var fakeStack;

  beforeEach(function () {
    fakeStack = new EventEmitter();
  });

  it('exports a function', function () {
    assert.equal(typeof (PenelopeStackClient), 'function');
  });

  it('requires a stack', function () {
    assert.throws(
      function () {
        new PenelopeStackClient();
      },
      /stack is required/
    );
  });

  it('returns a function', function () {
    var target = new PenelopeStackClient(fakeStack);

    assert.equal(typeof (target), 'function');
  });

  it('requires a socket', function () {
    var target = new PenelopeStackClient(fakeStack);

    assert.throws(
      function () {
        target();
      },
      /socket is required/
    );
  });

  it('returns a promise', function () {
    var target = new PenelopeStackClient(fakeStack);

    var socket = new EventEmitter();

    var r = target(socket);
    assert(r instanceof Promise);
  });

  it('listens subscribe', function (done) {
    var mockSocket = new EventEmitter();

    fakeStack.addSubscriber = function (socket, namespace) {
      try {
        assert.equal(socket, mockSocket);
        assert.equal(namespace, 'MyEvent');

        done();
      } catch (e) {
        done(e);
      }
    };

    var target = new PenelopeStackClient(fakeStack);

    target(mockSocket)
      .then(function () {
        mockSocket.emit('subscribe', 'MyEvent');
      });
  });

  describe('listens #describe-apis', () => {
    it('handles success', function (done) {
      var mockSocket = new EventEmitter();

      fakeStack.listPlugins = function () {
        return { 'StubPlugin1': { 'echo': ['what'] } };
      };

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket.emit('describe-apis', function (err, apis) {
            try {
              assert(!err);
              assert.deepEqual(apis, { 'StubPlugin1': { 'echo': ['what'] } });

              done();
            } catch (e) {
              done(e);
            }
          });
        });
    });
    
    it('handles error', function (done) {
      var mockSocket = new EventEmitter();

      fakeStack.listPlugins = function () {
        throw new Error('some error');
      };

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket.emit('describe-apis', function (err, apis) {
            try {
              assert.throws(() => { throw err; }, /some error/);

              done();
            } catch (e) {
              done(e);
            }
          });
        });
    });
  });

  describe('listens #describe-token-providers', () => {
    it('handles success', function (done) {
      var mockSocket = new EventEmitter();

      let fakeTokenProviders = {
        'tp1': {
          'm1': ['p1']
        }
      };

      fakeStack.listTokenProviders = () => fakeTokenProviders;

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket.emit('describe-token-providers', function (err, tokenProviders) {
            try {
              assert(!err);
              assert.deepEqual(tokenProviders, fakeTokenProviders);

              done();
            } catch (e) {
              done(e);
            }
          });
        });
    });

    it('handlers fail', function (done) {
      var mockSocket = new EventEmitter();

      fakeStack.listTokenProviders = () => {
        throw new Error('some error');
      };

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket.emit('describe-token-providers', function (err) {
            try {
              assert.throws(() => { throw err; }, /some error/);

              done();
            } catch (e) {
              done(e);
            }
          });
        });
    });
  });

  describe('listens api', function () {
    it('calls stack simpleInvokeMethod', function (done) {
      var mockSocket = new EventEmitter();

      fakeStack.simpleInvokeMethod = function (token, pluginName, methodName, methodParams, callback) {
        try {
          assert.deepEqual(token, { theToken: true });
          assert.equal(pluginName, 'StubPlugin1');
          assert.equal(methodName, 'echo');
          assert.deepEqual(methodParams, { 'what': 'this' });

          done();
        } catch (e) {
          done(e);
        }

        return new Promise(function () { });
      };

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket
            .emit(
              'api',
              { theToken: true },
              'StubPlugin1',
              'echo',
              { what: 'this' },
              function () { }
            );
        });
    });

    it('sends result to callback', function (done) {
      var mockSocket = new EventEmitter();
      let theResult = 'some result';

      fakeStack.simpleInvokeMethod = function (token, pluginName, methodName, methodParams, callback) {
        return Promise.resolve(theResult);
      };

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket
            .emit(
              'api',
              { theToken: true },
              'StubPlugin1',
              'echo',
              { what: 'this' },
              (err, result) => { 
                try {
                  assert.equal(result, theResult);

                  done();
                } catch (e) {
                  done(e);
                }
              }
            );
        });
    });
    
    it('sends result to callback', function (done) {
      var mockSocket = new EventEmitter();
      let theResult = 'some result';

      fakeStack.simpleInvokeMethod = function (token, pluginName, methodName, methodParams, callback) {
        return Promise.reject(new Error('some error'));
      };

      var target = new PenelopeStackClient(fakeStack);

      target(mockSocket)
        .then(function () {
          mockSocket
            .emit(
              'api',
              { theToken: true },
              'StubPlugin1',
              'echo',
              { what: 'this' },
              (err, result) => { 
                try {
                  assert.throws(() => { throw err; }, /some error/)

                  done();
                } catch (e) {
                  done(e);
                }
              }
            );
        });
    });
  });

  describe('#apis-changed event', function () {
    describe('on stack plugin-added event', function () {
      it('is emitted on plugin-added', function (done) {
        var mockSocket = new EventEmitter();

        fakeStack.listPlugins = function () {
          return { 'StubPlugin2': { 'what': ['echo '] } };
        };

        var target = new PenelopeStackClient(fakeStack);

        mockSocket
          .on('apis-changed', function (apis) {
            try {
              assert.deepEqual(apis, { 'StubPlugin2': { 'what': ['echo '] } });

              done();
            } catch (e) {
              done(e);
            }
          });

        target(mockSocket)
          .then(function () {
            fakeStack.emit('plugin-added');
          });
      });

      it('removes listener after socket disconnect event', function (done) {
        var mockSocket = new EventEmitter();

        var target = new PenelopeStackClient(fakeStack);

        target(mockSocket)
          .then(function () {
            mockSocket.emit('disconnect');

            try {
              assert.equal(0, fakeStack.listenerCount('plugin-added'));

              done();
            } catch (e) {
              done(e);
            }
          });
      });
    });

    describe('on stack plugin-removed event', function () {
      it('emits apis-changed', function (done) {
        var mockSocket = new EventEmitter();

        fakeStack.listPlugins = function () {
          return { 'StubPlugin2': { 'what': ['echo '] } };
        };

        var target = new PenelopeStackClient(fakeStack);

        mockSocket
          .on('apis-changed', function (apis) {
            try {
              assert.deepEqual(apis, { 'StubPlugin2': { 'what': ['echo '] } });

              done();
            } catch (e) {
              done(e);
            }
          });

        target(mockSocket)
          .then(function () {
            fakeStack.emit('plugin-removed');
          });
      });

      it('removes listener after socket disconnect event', function (done) {
        var mockSocket = new EventEmitter();

        var target = new PenelopeStackClient(fakeStack);

        target(mockSocket)
          .then(function () {
            mockSocket.emit('disconnect');

            try {
              assert.equal(0, fakeStack.listenerCount('plugin-removed'));

              done();
            } catch (e) {
              done(e);
            }
          });
      });
    });
  });

  it('listens unsubscribe', function (done) {
    fakeStack.removeSubscriber = function (socket, namespace) {
      try {
        assert.equal(socket, mockSocket);
        assert.equal(namespace, 'MyEvent');

        done();
      } catch (e) {
        done(e);
      }
    };

    var mockSocket = new EventEmitter();
    var target = new PenelopeStackClient(fakeStack);

    target(mockSocket)
      .then(function () {
        mockSocket.emit('unsubscribe', 'MyEvent');
      });
  });

  it('listens publish', function (done) {
    var mockSocket = new EventEmitter();

    fakeStack.publishEvent = function (eventName, token, message, socketFilter) {
      try {
        assert.equal(eventName, 'MyEvent');
        assert.deepEqual(token, { theToken: true });
        assert.deepEqual(message, { theMessage: true });
        assert(socketFilter({}));
        assert(!socketFilter(mockSocket));

        done();
      } catch (e) {
        done(e);
      }
    };

    var target = new PenelopeStackClient(fakeStack);

    target(mockSocket, { theToken: true })
      .then(function () {
        mockSocket.emit('publish', 'MyEvent', { theToken: true }, { theMessage: true });
      });
  });
});