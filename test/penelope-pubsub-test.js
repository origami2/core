var PenelopePubSub = require('../penelope-stack/pubsub');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('PenelopePubSub', function () {
  it('exports a function', function () {
    assert.equal(typeof (PenelopePubSub), 'function');
  });

  it('requires a stack', function () {
    assert.throws(
      function () {
        new PenelopePubSub();
      },
      /stack is required/
    );
  });

  it('listens to subscribe', function (done) {
    var mockSocket = new EventEmitter();

    var target = new PenelopePubSub({
      addSubscriber: function (socket, namespace) {
        try {
          assert.equal(socket, mockSocket);
          assert.equal(namespace, 'MyEvent');

          done();
        } catch (e) {
          done(e);
        }

        return Promise.resolve();
      }
    });

    target(mockSocket, {})
      .then(function () {
        mockSocket.emit('subscribe', 'MyEvent');
      });
  });

  it('listens to unsubscribe', function (done) {
    var mockSocket = new EventEmitter();

    var target = new PenelopePubSub({
      addSubscriber: function (socket, namespace) {
        return Promise.resolve();s
      },
      removeSubscriber: function (socket, namespace) {
        try {
          assert.equal(socket, mockSocket);
          assert.equal(namespace, 'MyEvent');

          done();
        } catch (e) {
          done(e);
        }

        return Promise.resolve();
      }
    });

    target(mockSocket, {})
      .then(function () {
        mockSocket.emit('subscribe', 'MyEvent', () => {
          mockSocket.emit('unsubscribe', 'MyEvent');
        });
      });
  });

  it('listens to publish', function (done) {
    var mockSocket = new EventEmitter();
    var theToken = {};
    var theMessage = {};

    var target = new PenelopePubSub({
      publishEvent: function (eventName, token, message, socketFilter) {
        try {
          assert.equal(eventName, 'MyEvent');
          assert.equal(token, theToken);
          assert.equal(message, theMessage);
          assert(socketFilter);

          assert(socketFilter({}));
          assert(!socketFilter(mockSocket));

          done();
        } catch (e) {
          done(e);
        }

        return Promise.resolve();
      }
    });

    target(mockSocket, {})
      .then(function () {
        mockSocket.emit('publish', 'MyEvent', theToken, theMessage);
      });
  });

  context('on disconnect', () => {
    it('removes subscribers', (done) => {
      var mockSocket = new EventEmitter();
      var subscribed = [];

      var target = new PenelopePubSub({
        addSubscriber: function (socket, namespace) {
          
          assert(socket === mockSocket);
          subscribed.push(namespace);

          return Promise.resolve();
        },
        removeSubscriber: function (socket, namespace) {
          assert(socket === mockSocket);

          subscribed.splice(subscribed.indexOf(socket), 1);

          return Promise.resolve();
        }
      });

      target(mockSocket, {})
        .then(function () {
          try {
            mockSocket.emit('subscribe', 'Event1', () => {
              mockSocket.emit('subscribe', 'Event2', () => {
                mockSocket.emit('disconnect');

                setTimeout(() => {
                  try {
                    assert.equal(subscribed.length, 0, 'still subscribed');
    
                    done();
                  } catch (e) {
                    done(e);
                  }
                }, 1000);
              });
            });
          } catch (e) {
            done(e);
          }
        });
    });
  });
});