var Stack = require('../stack');
var assert = require('assert');
var jsHelpers = require('../js-function-helpers');
var testData = {
  "plugin": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPC\nmR31TJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS6\n7C08YAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF\n9ARu5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGVi\nPlhtpImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUI\nNoYL8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQABAoIBAQCFX9Mqbc/jDLGb\n92+Q2eqTtMEiafE7bmznX5voa+bVk863h8eRGWj2FnG8aL2Cfq0EZRb9BKxCrJtQ\nL8fibtmaoLSYfxA4BOicQKnSozEWOhU47mBtiLQd9cDHf852l/VDsSzDfcm6jj9+\nF0UGB1zzmEnB/Fkue0HOzNKBGmbGE1/RWo4+co3XYpq/YZdIQQadsV6G2ma1IApm\n5t9AmEzhjMJsVFTZqzWQpoHmJRJjfwqqugmt4gpBCuYMkVcuN0g1olOLobF+pITn\nNjVaYimiFDsjhZyM4c0MFUYB27TeG7RV+QigHM/DW9rTbvTXxnTl/O4mipUe4FsK\nTdsbu4kBAoGBAPwJeudZz0cYM7MPzYE0/ksA9OrJwbhg+BjUJQ7CtFijJp8Cxt3d\ncu6DC5GOke0dXe845DLwriYKqJbae0FIeQvHrFTRpC2gGI5CJ3nPrZ3jbJhQLnQA\nMoF7EbJdpMM0I1xZsZ1cOy7jDbQ6/5Za4RC59xyqZ8hIi8vn9JVAkurPAoGBAIrd\nHyQ0NuZsQCxqNKPvU+1QnpFaoy65yA1kLZ/JDghjTiplxmv0WMuf1B3rq09RvTKK\nCEc7S8CredzLDtgQF7KiENnMno9e8bcMAddC+UaKsV9Eoy4mXGe8vTnQsGvlMkZI\n27gkM1o+Sbsl45Ayr3eYcpJD6K+txSjJYO8B3GIzAoGAUQ5ndYIftHinH95kNDqr\n0clj+yKZ58df4vRPWrjpsVv/LsKA3Je8v9JrZQuaCM0aCbadRXi8OUXSRHnNjAhX\nzZ8Q4FJv37COVSoXcgiFiLK8mRuoZOwvUg8XeOq+83yQJsI96iLgccrZ/G3BB0UA\n/xUf0RtIt1QFibV2po2W8mcCgYBjLOXO35POIcX7cqbB5m3Ucd2uBkPBXWIpXkDP\ne7KP/wyWbzW1aD/6vd2quOQStFghvj+HUCwcINvZ+xRQ771dES5jvyYHU1Hi36p4\n6RZLcUaYudapYTBhzoR+xDMb/AdZ9zMlYoVikFXsWXUbSXfUPIanO+T1g2/qX1jh\nmjyhzQKBgQCvQdWSYMXaOnQRo/BHmRdR3X3aEZyLEn5UmXjxohUeB/Jtzb15FD/r\ny5ljkTVQyzYnRgwBuZBnUl/sCbxRp1CXbDmotHZ3DBwRJuP7KaPoj7dH+xfU6P3N\nuAm6CP+73AONmouW/8tVW60n2IkSH5I1ROYtPLrI6475M/hB4Anj+Q==\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPCmR31\nTJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS67C08\nYAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF9ARu\n5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGViPlht\npImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUINoYL\n8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQAB\n-----END RSA PUBLIC KEY-----"
  },
  "tokenProvider": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqgeAsH\nZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jkbFZJ\nJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQKKx5\nVFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjOkSB8\ncUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdix0/H\nBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqg\neAsHZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jk\nbFZJJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQ\nKKx5VFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjO\nkSB8cUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdi\nx0/HBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQABAoIBAG0m0AHi5HyJ5SbU\nxNJ46LTBDAX3DkoNkQgRsUGAQV6sMOKVbEYa0SI5wyZEKfuGVaZYUv5mDBqL/ka9\nYkkaatqj/97TvMOoyt8tH8nIowuXyF7xNSH4LeEUZLqDr9VkYJhk38RPDMuc99fp\npsEZsTpmC56ygRZS/cVObZjefoKkl+2cPL0jfSWUSn2t/nUKbmwNbjbM6f1OCCyJ\n814LZalfeLh+hfEdY8yVK0Rlaut/FLw4eaY43H0QTwDyYXmgOgYckia7MQmtu5yO\n0dFRDZa7x0j01w0+Hphr88bhtXFgv8VPWyzyz2d8fNWlbMP4JCARes3lVpkMKHDn\nFZIULcECgYEA53qsuOQrE3xHtNzfak7FjGgkQTxi4TeYXRJ5Zhlkjtb9nQnsFzTq\n+AXpeHfsGmaza9/LcMUJ9JzYV7t5V8T6vZT3riOnjQAfbHOrR3EYK5/vv4ZfjFgk\nBySoNjOtf67ZXn1lRJ3FZNN4RfGmh/2aqYi5PAn1g5+nLXtMIY88a1ECgYEAxGp9\nW03pH4QwBzvFA9PJ+BhWXS8fAcafY67TlXg+IxzGP/YQupodovDChXpvbfYV0kf6\nU6YddDQT5p0j2YmssJCaDa93iMXU2/JgI5pcTNFxLa5LR/Mg30JYyniK/4+YIkoL\n68V2OH0Y65DIQdajHPojzFBmDSBZSnkyO9Ojx2MCgYADCoSMZUr/lYlnoeM5hVFp\nF9EqHj36UX2p810u7zR3//ETCBdW8rYHjiRUFdc/PYwr5aPJln0b/peFB4x/j7Hv\nna5nVkaUPqUrCpX8eUrk/9Ppgz1sHZhTk7K2C5XC8KwgZqtW7G+0dGbHHHagoL9Q\nbOBqHoNgOE+89Dq60iPsEQKBgByahXbueaylS3lCMwbDqP4ATVN0sUdI7Z1OsHFr\n+WCTqCtYYkdKelZoSWu20NNqqvLcmI/l+RQbIWrMJ5RegE+WP1kO3JGGfeEqAuYs\nbJSjS6AjacMonPjmaJfTxipBdx5HOkUzlGvVi/OCOiecYlSt+NigPLxcoaQ+0hn0\nUD2RAoGAUH2QV31iSFLs+ByQdGmPaYuVxhNUwaGoN0M5splIdH6DAgyIvM5ogXVb\nMV3yUdnULU+5YeL8r93NeFRHiBCBfvcIcDdJHxRJNQFvdsAWosGeXI6clC1YkUnB\n2N+9PrizN87XUb6NmY07NwtAKrU6aZ0XwKUEgor9rntT8IawFo4=\n-----END RSA PRIVATE KEY-----"
  },
  "stack": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4\nPKtgrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQae\nsmwcu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIi\nGaR/A/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTE\nS+NTkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylh\nX2+xsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQABAoIBAQCzPkfPKGiXj/ho\npviEbfmfR9W2w0F+1V03Wk1Rjo8whxH6Rg9DuJpJcDOza95W2B+uz0Ds64Z6av0j\nmiK8SU4T8S8Yl8merXzHKCdq0IUBzRRan2s/48dsaRhn+G+wSs5yvQWAz0eXHGhR\n2xaPvXSTZdqnPzF/mrgUC3mp538a3TuqNzJYfowY4d+Rn6+8K5EqcWHHBBINBp90\nhWPxGMp2mTFDsycQaQjDsfGJDAlFtQJ0FlrU0KAHt5KhaX7jMvOvD7AuL8aEnnWy\n9yTwb+POpPxrz1a+6p4U7YIPef3yVmWS+QUI2aDtgz7LMlxyTfkX4OiwmBGw51ED\niIwKCYwBAoGBAOfD1WkLhjqAqPDYdm1VkRYQILDWv6vUhznevw/J8yItnpYakRe7\njmhdiQh/p3ieOaA/l31p2ZR4OGT2hZwefAFYbwVoCOAtoIelbJwz5Mu1H7mMBuav\n23/K41edQ25zRbEjy1f20CMBHbEhF+64ufRGUUnJcA0gV9dO4ZvdvQ0jAoGBAMju\nwM/T0sTM83l1IPRvd7moTKzhOt2dJXq660PVY1FqLKSvXicCsFQtEd4J2K2Aik+n\n7A9m0DJx/QauPZ3Dk2K1dWCXqTRp7Yjhvpmw2mvrTfRNFww+Ths15XDCAtRWzUSV\n8yB9TnTH8opeUejWcoI8VikhVtkR8hF5m0xhGXEZAoGAYe4Rwu0nrpimyf3tLI23\nXIc8CPz0yHppGT7RpK5EmfCEzhAztr99kQPU305xSToyR1AXhtqvIVkbGy4/jpQi\n+b6QSsyG05Lz/d0cY3RZ/OlvmktcryUnrnvgTCkbURRMImlphqW3lNLn5OyC3FAZ\n4unDd1YyjplYBJZEJkQvmdkCgYBbtV0bfjq8yC54SRV5e0bR3hbg8QvFCmyIz7eB\nhmuGRUeLAt+ePoPsZ4W4KhZTWk/Ge4YqoWp1G8G4wt4sm48xhlDEfXQlEBPyXdZ4\nn1eR2hwCXQ0f8XAEy1ylUmeoMtYNb8NAGPEuK/RvYw1PkFFDT/ajXywcdyxc/Xv8\nKHVMQQKBgFPnGNRfLtCVfONwcveqfUeR4xS4UJkTNjLExTzBRKoUuKLbZwV5tHEr\nBFoMaM3V5pPLKfImO4BYZeuPdB1DBG3dsxL5G5TzWBauJq7OGAQmXuHecV4pblHm\nPO9yEktLnn8QAkbiePv+PhFFnSAa93cYHNtM68bBTdLrohBzZbDW\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4PKtg\nrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQaesmwc\nu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIiGaR/\nA/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTES+NT\nkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylhX2+x\nsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQAB\n-----END RSA PUBLIC KEY-----"
  },
  "guard1": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAoaOMFD6QdNZcLScNH084z9tSEsffK2+6L7HRwQEFm9VeRJXpSG1J\n+10YL/jSp/sljiusiePlWPiO+Jxz7ENH625iz6CkacEa3J8nk6EVQM8IeCnyWkh4\nLQXppbFg3emA86ZTh4RFfqWuS4w4i/ouhPlWBMDzQL2xtl4nHlry3wEYfMrBvZcD\nG55azifbFhGtX7zpQcTrzobV+eGvIdFq8JhdSqe8e3z3yYngKkGX7cpYGjlfzJmB\nbX2hj5K/6BpV8odgVtB9AoaWv6a+JkNnp7PdyPvM0Mp/Fy7NsqTc2LRPuFQGr1wf\nbawZL3YHgLhrvECk5t0PT7WSCVPe6wS0TQIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAoaOMFD6QdNZcLScNH084z9tSEsffK2+6L7HRwQEFm9VeRJXp\nSG1J+10YL/jSp/sljiusiePlWPiO+Jxz7ENH625iz6CkacEa3J8nk6EVQM8IeCny\nWkh4LQXppbFg3emA86ZTh4RFfqWuS4w4i/ouhPlWBMDzQL2xtl4nHlry3wEYfMrB\nvZcDG55azifbFhGtX7zpQcTrzobV+eGvIdFq8JhdSqe8e3z3yYngKkGX7cpYGjlf\nzJmBbX2hj5K/6BpV8odgVtB9AoaWv6a+JkNnp7PdyPvM0Mp/Fy7NsqTc2LRPuFQG\nr1wfbawZL3YHgLhrvECk5t0PT7WSCVPe6wS0TQIDAQABAoIBABfa0ymJsjgMCr5/\nLOiaAIBslcg/xyfZ7VhsndAmHnXJH4hvt2YA+xIlwxQRXMV9wyUgPO58u7RNuovm\nSxVrOIn+WIbb5mfVH+m/c0/OThyqcU2Ko0G/wDykyr6xVuJpO6a5vUoas0blnLt5\njj7aRVNKmO9Kqpu7YybO7mcEdSOEAdWgcgA5/4CA66PRq9twx8WKG52ObUGJtw1z\nXy/d/9/NzFkKQpkDUpYE6HZCr2Q0gzeVlf8WAvsxxnexPGXf+EQldrFrH5pWjwvV\nJ8Qtl7Zu+YT2Vm746ZExRwa4BxX5yoE2f/OR722jGwcTsz5gQZxbXhOK+3A5eR2Z\nhjV16eECgYEAzUfG7V/TovnlN+3VrDsUemjFTl16QREdA/h1SvCRxuzwemuTEnsH\n0djUKM1jziyWOETl26aihO3g3NElnqbqln23VY+hJ//Uk3dKK85m38Zh1zPwSXcW\nqegphAWEJnDHhrVXNTTeIHo7W4T10X0oz0wovPRI8u6MM11al22TzNMCgYEAyZNk\nm2OWEbhDS9RSnXZ/wsjByosXxg5TyH/FadLiesg3jSU0ipOs9CUGODtTytfBzBXj\nJL1BrXxPcAHE5VqxLGVqVEiDXi7Jetr9Zxykhu02gAUP3q8NmTQ9LsjkNiQkrFi3\nwi2UJljMnue6l1yc844HiYm8lMcXwm2mmQnuRl8CgYAGoyZ8cFfAckIGD+uTuQlN\nd+vBP+Z/ChWdFkB8ntCn1OtlmerUcyPqlg8uDo4GkYx930RrWLTyhVp66UxFF/KH\n0SLHYnB8gZoW9olwZykOXTS2p5hCV9Zkwfvtp9wA/i6FXYGnV8FC3ZDjwaXNEzXp\nlfXPPjrJmArycjdGpCVePwKBgC76pRX6Db2vfi+sGl0MgxY4h8HWzQGh0TEHZlEV\njycihp/krewmIsb74gk0+oBp7IbOCxRC742pTTJDl0oE6wn30OI3zDYlHVv+sdRY\nPpgbrPoFM/+oOLkeDaxbsaRUpB9uxdUBnmeZhob6Zuhj80hoL6WEbmkWRZlQyOUe\nBWRtAoGBAJlAI062MjTOQYG4if9PKBulbBnVDG6FH+xxso7t2KwndR8jcU5Bl4U2\nWckFJS6t0KWsT/ydp78cxEoofTviPJuCOF4KuKcForYEpgp4hKEbkKx6666zNorR\njxvJZaWLyiqe5kE+mTVC/cDlkHEK96pXY0RoafS3v4aBBBSIp+yJ\n-----END RSA PRIVATE KEY-----"
  },
  "guard2": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEA8Y4ea04ZNvHNhWJjJqsaj2n4ryyOK4sPY+uXt3MO6CIOxosSG3sY\nP/w6VRxpixxwuQoXZninEDLwpRDTTNVVuernMeUQDPnhhrscfJbRLDeI3Iqn5U9t\ny+atjbZ40dNCrSrsjOUsdY4RoSxvxe9DNuA+jx/1nvZVSgN9N9Wg9oQCVU4h4CQ0\nAB9VIIPVQ+niTQlhQKdeDxlJzZ14T8SIsxSZ3obqVggK10Lc9JpEH+5dPzRRlBs8\n5XNmidkeQkObYHuyFaG35ZB2yZtPib1iDklD+TAKZiTXtCEHdEFLs+mBnm61LbbW\nW+3AXvltY0pW78mDMuP9SlRUn0t97+b7NQIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEA8Y4ea04ZNvHNhWJjJqsaj2n4ryyOK4sPY+uXt3MO6CIOxosS\nG3sYP/w6VRxpixxwuQoXZninEDLwpRDTTNVVuernMeUQDPnhhrscfJbRLDeI3Iqn\n5U9ty+atjbZ40dNCrSrsjOUsdY4RoSxvxe9DNuA+jx/1nvZVSgN9N9Wg9oQCVU4h\n4CQ0AB9VIIPVQ+niTQlhQKdeDxlJzZ14T8SIsxSZ3obqVggK10Lc9JpEH+5dPzRR\nlBs85XNmidkeQkObYHuyFaG35ZB2yZtPib1iDklD+TAKZiTXtCEHdEFLs+mBnm61\nLbbWW+3AXvltY0pW78mDMuP9SlRUn0t97+b7NQIDAQABAoIBAQDiRh4KqKMIDCRY\nkVyN6XQ86ajx90vKUD5/fD6nsxlArD6pD7dLlNiXf5/pAG+Bx4d0Xpai/9yHWSCB\nj6xP6fue6APgglGxxhXUDszVjLWutOr4pM44CVqxyYnEGS0+aKap7m6eYUNvb0Dm\neOjPQpz098TCb863NMuaRIB04CIjKJ3ZC1FSKgLOO5AnFqruedvw73v0R5GowbTu\n5Idcou2sWHbYn2ZVaP0LesJ19GMf5SX/2rQrfcp98K2ZZ5aBypyr3Pg0jvBPDbXL\ndh+sV/QlORCrLd+36ilEZw3kuQoS94qaTIuJMcZefQDcC2iIHu3YkP8N2iSVZTY5\nOSaQ7c71AoGBAPs2cCLbNLA0LOKnOr1sS97JD/cHwknFpeBe3B0SJ73A1FhZIw59\nEwrN9SBjef1PoMPlQRf9Ufjch/xfFjC7ucSFMMdyCz/FSHuEdj+ptO7G7mkiq0xk\nV5LxSmZRKR0xxzL5weglRPzE9ro3PhA36X6koK0Nvej4cIqockymiMaDAoGBAPYo\nkN3Q17mjetvHsMJRvIDDGQyIJtYLVDR1bwb11DozVv+PTATkvXZYbQhagQUly4/7\nO+/gD7wKPaLpdWVBx2hENPx9EabANWnGMI/yNzoskwd9PatWv86eYgsm1/O+qb30\nGroNYXWkaV9DeUaHQgukDDgshk739NiFFuaktcnnAoGARiGUfKaltutZ/IyjIW6y\nX6fSBSztWJsNC+5vlM4UuiyzG1cHKmxQfJuy6y/aRpWMVCQUYBYgZFFsxU/magR+\ngBaO358+9Lam3ZTFTlCaam6u8Dr1h4YbzLIAC0BtMfFLozeGosCE2exC1QnHEdAK\nJ4jeMnG7quZwxMTx4+Q6GvECgYAxKZ7tPkWnhNgHxxUtaiesy8pX72VaQH77zyxy\nhmxxh0lr1xHGtGyGk1iLX4//GDJUukeZO/XIh38R53CT3ZRW5bBzcQMye5bvXi7a\nwPR4wOhIiaHkc3nxaT6JnaNpwf2iu3sPB5ccaM7GwPIavO7a5lOucG4OnYUgLUyK\nmgDYpwKBgG5NfQDt+LYDC13HB2K5+M+FdHsSpCGrUBbJlokbN4OaINyKZEZJMFUS\nuZzJ1pwaCPxklzsI2ADVpDGR89CRvgn4yAb3+Fx5xUKdthkGhbq1yiI80xfDBiEi\n6Rp3trB7FdJa2l8O0JNhMfr/fx0e0p/j9fsqVjaZ5uSHa0ST9UmE\n-----END RSA PRIVATE KEY-----"
  }
};
var crypto = require('../crypto-utils');
var ConnectedEmitters = require('../emitters-bus');
var EventEmitter = require('events').EventEmitter;

function et2s(what) {
  return crypto
    .encryptAndSign(
      what,
      testData.tokenProvider.privateKey,
      testData.stack.publicKey
    );
}

function dt2s(what) {
  return crypto
    .decryptAndVerify(
      what,
      testData.stack.privateKey,
      testData.tokenProvider.publicKey
    );
}

var fakeContext = {
  username: 'myuser',
  fold: 'fold1'
};
var stackToken = crypto.encryptAndSign(
  fakeContext,
  testData.stack.privateKey,
  testData.stack.publicKey
);


describe('stack', function () {
  var target;

  it('requires private key to instantiate', function () {
    assert.throws(
      function () {
        new Stack();
      },
      /private key is required/
    );
  });

  beforeEach(function () {
    target = new Stack(
      testData.stack.privateKey,
      testData.stack.publicKey
    );
  });

  it('inherits EventEmitter', function () {
    assert(target instanceof EventEmitter);
  });

  describe('.getPublicKey', function () {
    it('returns public key', function () {
      assert.equal(testData.stack.publicKey, target.getPublicKey());
    });
  });

  describe('.addLocalPlugin', function () {
    it('requires plugin', function () {
      assert.throws(
        function () {
          target.addLocalPlugin();
        },
        /plugin is required/
      );
    });

    it('accepts plugin', function () {
      target.addLocalPlugin({
        describeMethods: function () {
          return {
            'echo': ['what']
          };
        },
        getName: function () {
          return 'Plugin1';
        }
      });

      assert.deepEqual(
        target.listPlugins(),
        {
          'Plugin1': {
            'echo': ['what']
          }
        }
      );
    });

    it('emits plugin-added', function (done) {
      target.on('plugin-added', function (pluginName, methods) {
        try {
          assert.equal(pluginName, 'Plugin1');
          assert.deepEqual(methods, { 'echo': ['what'] });

          done();
        } catch (e) {
          done(e);
        }
      });

      target.addLocalPlugin({
        describeMethods: function () {
          return {
            'echo': ['what']
          };
        },
        getName: function () {
          return 'Plugin1';
        }
      });
    });

    it('invokes plugin api', function (done) {
      target
        .addLocalPlugin({
          invokeMethod: function (context, methodName, methodParams) {
            return Promise.resolve(methodParams.what + ' ' + context.username);
          },
          describeMethods: function () {
            return {
              'echo': ['what']
            };
          },
          getName: function () {
            return 'Plugin1';
          }
        });

      target
        .invokeMethod(null, { username: 'you' }, 'Plugin1', 'echo', { what: 'hello' })
        .then(function (result) {
          try {
            assert.equal(result, 'hello you');

            done();
          } catch (e) {
            done(e);
          }
        });
    });
  });

  describe('.getPrivateKey', function () {
    it('returns public key', function () {
      assert.equal(testData.stack.privateKey, target.getPrivateKey());
    });
  });

  it('lists no subscribers at first', function () {
    assert.deepEqual([], target.listSubscribers('some.change'));
  });

  it('lists no trusted subscribers at first', function () {
    assert.deepEqual([], target.listTrustedSubscribers('some.change'));
  });

  it('lists no guards at first', function () {
    assert.deepEqual([], target.listGuards());
  });

  it('lists no token providers at first', function () {
    assert.deepEqual({}, target.listTokenProviders());
  });

  var fakeSubscriber;

  beforeEach(function () {
    fakeSubscriber = new EventEmitter();
  });

  describe('.addSubscriber', function () {
    it('adds a subscriber', function () {
      target.addSubscriber(fakeSubscriber, 'some.change');

      assert
        .deepEqual(
          [fakeSubscriber],
          target.listSubscribers('some.change')
        );
    });

    it('returns Promise', function () {
      var promise = target.addSubscriber(fakeSubscriber, 'some.change');

      assert(
        promise instanceof Promise
      );
    });
  });

  describe('.addTrustedSubscriber', function () {
    it('adds a subscriber', function () {
      target.addTrustedSubscriber(fakeSubscriber, 'some.change');

      assert
        .deepEqual(
          [fakeSubscriber],
          target.listTrustedSubscribers('some.change')
        );
    });
  });

  describe('.publish', function () {
    it('publishes to subscribers', function (done) {
      target.addSubscriber(fakeSubscriber, 'some.change');

      fakeSubscriber
        .on('event', function (eventName, message) {
          try {
            assert.equal('some.change', eventName);
            assert.deepEqual({
              magic: 'thing'
            }, message);

            done();
          } catch (e) {
            done(e);
          }
        });

      target.publishEvent('some.change', stackToken, { magic: 'thing' });
    });

    it('publishes to trusted subscribers', function (done) {
      target.addTrustedSubscriber(fakeSubscriber, 'some.change');

      fakeSubscriber
        .on('event', function (eventName, token, context, message) {
          try {
            assert.equal('some.change', eventName);
            assert.deepEqual(token, stackToken);
            assert.deepEqual({
              magic: 'thing'
            }, message);

            done();
          } catch (e) {
            done(e);
          }
        });

      target.publishEvent('some.change', stackToken, { magic: 'thing' });
    });

    it('uses socket filter publishes to subscribers', function (done) {
      target.addSubscriber(fakeSubscriber, 'some.change');

      fakeSubscriber
        .on('event', function (namespace, message) {
          done('this shouldn\'t have happened');
        });

      target
        .publishEvent(
          'some.change',
          stackToken,
          { magic: 'thing' },
          function () {
            return false;
          }
        );

      done();
    });

    it('double addSubscriber to not publishes twice', function () {
      target.addSubscriber(fakeSubscriber, 'some.change');
      target.addSubscriber(fakeSubscriber, 'some.change');

      var times = 0;
      fakeSubscriber
        .on('event', function (namespace, token, message) {
          times++;
        });

      target.publishEvent('some.change', stackToken, { magic: 'thing' });
      assert.equal(1, times);
    });

    describe('.removeSubscriber', () => {
      it('removes subscriber', function () {
        var fakeSubscriber2 = new EventEmitter();

        target.addSubscriber(fakeSubscriber, 'some.change');
        target.addSubscriber(fakeSubscriber2, 'some.change');

        var called1;
        var called2;

        fakeSubscriber
          .on('event', function (namespace, token, message) {
            called1 = true;
          });

        fakeSubscriber2
          .on('event', function (namespace, token, message) {
            called2 = true;
          });

        target.removeSubscriber(fakeSubscriber2, 'some.change');
        target.publishEvent('some.change', stackToken, { magic: 'thing' });

        assert(called1);
        assert(!called2);
      });

      it('returns a Promise', function () {
        var fakeSubscriber = new EventEmitter();

        target.addSubscriber(fakeSubscriber, 'some.change');

        var result = target.removeSubscriber(fakeSubscriber, 'some.change');
        
        assert(result instanceof Promise);
      });
    });

    context('after socket disconnection', function () {
      it('skips disconnected socket', function (done) {
        target.addSubscriber(fakeSubscriber, 'some.change');

        fakeSubscriber
          .on('event', function (namespace, token, message) {
            done(new Error('this shouldn\'t have been called'));
          });

        fakeSubscriber
          .emit('disconnect');

        target.publishEvent('some.change', stackToken, { magic: 'thing' });

        done();
      });
    });
  });

  it('lists no plugins at first', function () {
    assert.deepEqual({}, target.listPlugins());
  });

  var stubPlugin1;

  beforeEach(function () {
    stubPlugin1 = new EventEmitter();
  });

  describe('.addPlugin', function () {
    it('adds a plugin', function () {
      target.addPlugin('StubPlugin1', stubPlugin1, {});

      assert.deepEqual({}, target.listPlugins()['StubPlugin1']);
    });

    it('emits plugin-added', function (done) {
      target.on('plugin-added', function (pluginName, methods) {
        try {
          assert.equal(pluginName, 'StubPlugin1');
          assert.deepEqual(methods, { 'echo': ['what'] });

          done();
        } catch (e) {
          done(e);
        }
      });

      target.addPlugin('StubPlugin1', stubPlugin1, { 'echo': ['what'] });
    });
  });

  describe('.invokeMethod', function () {
    it('rejects invalid plugin', function (done) {
      target
        .invokeMethod(null, {}, 'IDoNotExist', 'NeitherIDo', {})
        .catch(function (error) {
          try {
            assert.equal(error, 'invalid plugin');

            done();
          } catch (e) {
            done(e);
          }
        });
    });

    it('rejects invalid method', function (done) {
      var socket = new EventEmitter();

      target
        .addPlugin('StubPlugin1', socket, { 'echo': ['what'] });

      target
        .invokeMethod(
          null,
          {},
          'StubPlugin1',
          'thisMethodDoesNotExists',
          {
          }
        )
        .catch(function (error) {
          try {
            assert.equal(error, 'invalid method');

            done();
          } catch (e) {
            done(e);
          }
        });
    });

    it('emits api on socket', function (done) {
      var socket = new EventEmitter();

      socket
        .on(
          'api',
          function (stackToken, context, methodName, methodParams, callback) {
            try {
              assert(!stackToken);
              assert.deepEqual(context, {});
              assert.equal(methodName, 'echo');
              assert.deepEqual(methodParams, { 'what': 'this' });
              assert.equal(typeof (callback), 'function');

              done();
            } catch (e) {
              done(e);
            }
          }
        );

      target
        .addPlugin('StubPlugin1', socket, { 'echo': ['what'] });

      target
        .invokeMethod(
          null,
          {},
          'StubPlugin1',
          'echo',
          {
            'what': 'this'
          }
        )
        .catch(() => {
          // YOLO
          // we expect this function to fail
        });
    });

    it('resolves promise on successfull callback', function (done) {
      var socket = new EventEmitter();

      socket
        .on(
          'api',
          function (stackToken, context, methodName, methodParams, callback) {
            callback(null, 'that');
          }
        );

      target
        .addPlugin('StubPlugin1', socket, { 'echo': ['what'] });

      target
        .invokeMethod(
          null,
          {},
          'StubPlugin1',
          'echo',
          {
            'what': 'this'
          }
        )
        .then(function (result) {
          try {
            assert.equal(result, 'that');

            done();
          } catch (e) {
            done(e);
          }
        });
    });

    it('reject promise on failed callback', function (done) {
      var socket = new EventEmitter();

      socket
        .on(
          'api',
          function (stackToken, context, methodName, methodParams, callback) {
            callback('the error');
          }
        );

      target
        .addPlugin('StubPlugin1', socket, { 'echo': ['what'] });

      target
        .invokeMethod(
          null,
          {},
          'StubPlugin1',
          'echo',
          {
            'what': 'this'
          }
        )
        .catch(function (error) {
          try {
            assert.equal(error, 'the error');

            done();
          } catch (e) {
            done(e);
          }
        });
    });
  });

  it('drops a plugin if it disconnects', function (done) {
    stubPlugin1.emit('disconnect');

    assert.deepEqual(undefined, target.listPlugins()['StubPlugin1']);

    done();
  });

  describe('.authorizeGuardPublicKey', function () {
    it('accepts a valid key', function () {
      target
        .authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);

      assert.deepEqual(['StubGuard1'], target.listGuards());
    });

    it('accepts a valid key', function () {
      target
        .authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);

      assert.deepEqual(['StubGuard1'], target.listGuards());
    });

    it('accepts a valid key', function () {
      assert.throws(
        function () {
          target
            .authorizeGuardPublicKey(
              null,
              testData.guard1.publicKey
            );
        },
        /guard name is required/
      );
    });

    it('rejects an invalid key', function () {
      assert.throws(
        function () {
          target
            .authorizeGuardPublicKey(
              'invalidGuard',
              'invalid key'
            );
        },
        /invalid key/
      );
    });
  });

  describe('.addClient', function () {
    it('requires socket', function () {
      assert
        .throws(
          function () {
            target
              .addClient();
          },
          /socket is required/
        );
    });

    describe('.removeSocket', function () {
      it('removes all listeners', function (done) {
        var stubClient = new EventEmitter();

        target
          .addClient(stubClient);

        target.removeSocket(stubClient);

        try {
          assert.equal(0, stubClient.listeners().length);

          done();
        } catch (e) {
          done(e);
        }
      });
    });

    describe('describe-token-providers event', function () {
      it('listents describe-token-providers', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        target
          .addTokenProvider(
            'StubTokenProvider1',
            stubTokenProvider,
            {
              'authenticate': ['username', 'password']
            },
            testData.tokenProvider.publicKey
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'describe-token-providers',
            function (err, result) {
              try {
                assert(!err);
                assert.deepEqual(
                  {
                    'StubTokenProvider1': {
                      publicKey: testData.tokenProvider.publicKey,
                      methods: {
                        'authenticate': ['username', 'password']
                      }
                    }
                  },
                  result
                );

                done();
              } catch (e) {
                done(e);
              }
            }
          );
      });
    });

    describe('api event', function () {
      it('listens api', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin3 = new EventEmitter();

        stubPlugin3
          .on(
            'api',
            function (
              token,
              context,
              method,
              args,
              callback
            ) {
              try {
                assert.equal(token, stackToken);
                assert.deepEqual(
                  {
                    'fold': 'fold1',
                    'username': 'myuser'
                  },
                  context
                );
                assert.equal('echo', method);
                assert.deepEqual(
                  {
                    what: 'this'
                  },
                  args
                );
                assert(callback);

                done();
              } catch (e) {
                done(e);
              }
            }
          );

        target
          .addPlugin(
            'StubPlugin3',
            stubPlugin3,
            {
              'echo': ['what']
            }
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'api',
            stackToken,
            'StubPlugin3',
            'echo',
            {
              what: 'this'
            },
            function (err, result) {

            }
          );
      });

      it('accepts no token, gives no context', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin3 = new EventEmitter();


        stubPlugin3
          .on(
            'api',
            function (
              token,
              context,
              method,
              args,
              callback
            ) {
              try {
                assert.equal(null, token);
                assert.deepEqual(
                  {
                  },
                  context
                );
                assert.equal('echo', method);
                assert.deepEqual(
                  {
                    what: 'this'
                  },
                  args
                );
                assert(callback);

                done();
              } catch (e) {
                done(e);
              }
            }
          );

        target
          .addPlugin(
            'StubPlugin3',
            stubPlugin3,
            {
              'echo': ['what']
            }
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'api',
            null,
            'StubPlugin3',
            'echo',
            {
              what: 'this'
            },
            function (err, result) {

            }
          );
      });

      it('passes unsuccessful result back', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin4 = new EventEmitter();

        stubPlugin4
          .on(
            'api',
            function (
              token,
              context,
              method,
              args,
              callback
            ) {
              callback('my error');
            }
          );

        target
          .addPlugin(
            'StubPlugin4',
            stubPlugin4,
            {
              'echo': ['what']
            }
          );

        target
          .addClient(stubClient);
        stubClient
          .emit(
            'api',
            stackToken,
            'StubPlugin4',
            'echo',
            {
              what: 'this'
            },
            function (err, result) {
              try {
                assert.equal('my error', err);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
      });

      it('passes successful result back', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin5 = new EventEmitter();

        stubPlugin5
          .on(
            'api',
            function (
              token,
              context,
              method,
              args,
              callback
            ) {
              callback(null, 'my result');
            }
          );

        target
          .addPlugin(
            'StubPlugin5',
            stubPlugin5,
            {
              'echo': ['what']
            }
          );

        target
          .addClient(stubClient);
        stubClient
          .emit(
            'api',
            stackToken,
            'StubPlugin5',
            'echo',
            {
              what: 'this'
            },
            function (err, result) {
              try {
                assert.equal('my result', result);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
      });
    });

    describe('get-token event', function () {
      it('passes call', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        stubTokenProvider
          .on(
            'get-token',
            function (
              stackToken,
              context,
              methodName,
              params,
              callback
            ) {
              try {
                assert(stackToken === null);
                assert.deepEqual(
                  {
                  },
                  context
                );
                assert.equal('authenticate', methodName);
                assert.deepEqual(
                  {
                    username: 'myuser',
                    password: 'secret'
                  },
                  params
                );

                assert(callback);

                done();
              } catch (e) {
                done(e);
              }
            }
          );

        target
          .addTokenProvider(
            'StubTokenProvider1',
            stubTokenProvider,
            {
              'authenticate': ['username', 'password']
            },
            testData.tokenProvider.publicKey
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'get-token',
            null,
            'StubTokenProvider1',
            'authenticate',
            {
              username: 'myuser',
              password: 'secret'
            },
            function (err, result) {
            }
          );
      });

      it('passes successful response', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        stubTokenProvider
          .on(
            'get-token',
            function (
              stackToken,
              context,
              methodName,
              params,
              callback
            ) {
              callback(
                null,
                {
                  username: 'myuser',
                  permissions: [
                    'somepermission'
                  ]
                }
              );
            }
          );

        target
          .addTokenProvider(
            'StubTokenProvider1',
            stubTokenProvider,
            {
              'authenticate': ['username', 'password']
            },
            testData.tokenProvider.publicKey
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'get-token',
            null,
            'StubTokenProvider1',
            'authenticate',
            {
              username: 'myuser',
              password: 'secret'
            },
            function (err, result) {
              try {
                assert(!err);
                assert(result);

                var decrypted = crypto.decryptAndVerify(
                  result,
                  testData.stack.privateKey,
                  testData.stack.publicKey
                );

                assert.deepEqual(
                  {
                    username: 'myuser',
                    permissions: [
                      'somepermission'
                    ]
                  },
                  decrypted
                );

                done();
              } catch (e) {
                done(e);
              }
            }
          );
      });

      it('passes error response', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        stubTokenProvider
          .on(
            'get-token',
            function (
              stackToken,
              context,
              methodName,
              params,
              callback
            ) {
              callback(
                'some error'
              );
            }
          );

        target
          .addTokenProvider(
            'StubTokenProvider1',
            stubTokenProvider,
            {
              'authenticate': ['username', 'password']
            },
            testData.tokenProvider.publicKey
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'get-token',
            null,
            'StubTokenProvider1',
            'authenticate',
            {
              username: 'myuser',
              password: 'secret'
            },
            function (err, result) {
              try {
                assert.equal('some error', err);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
      });
    });

    describe('describe-apis event', function () {
      it('listens event', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin3 = new EventEmitter();

        target
          .addPlugin(
            'StubPlugin3',
            stubPlugin3,
            {
              'echo': ['what']
            }
          );

        target
          .addClient(stubClient);

        stubClient
          .emit(
            'describe-apis',
            function (err, result) {
              try {
                assert(!err);

                assert({
                  'StubPlugin3': {
                    'echo': ['what']
                  }
                }, result);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
      });
    });
  });

  describe('.addGuardCheck', function () {
    var guardSocket;

    it('adds valid code', function (done) {
      guardSocket = new EventEmitter();

      target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
      target.addGuard('StubGuard1', guardSocket);

      try {
        function check1(reject) {
          if (fold === 'fold1' && plugin === 'StubPlugin1' && method === 'echo') return;

          reject();
        }

        target
          .addGuardCheck(
            'StubGuard1',
            'check1',
            jsHelpers.getFunctionBody(check1)
          );

        assert.deepEqual({
          'StubGuard1': ['check1']
        }, target.listGuardChecks());

        done();
      } catch (e) {
        done(e);
      }
    });

    describe('.runCheck', function () {
      it('checks', function (done) {
        guardSocket = new EventEmitter();

        target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
        target.addGuard('StubGuard1', guardSocket);

        function check1(reject) {
          if (fold === 'fold1' && plugin === 'StubPlugin1' && method === 'echo') return;

          reject();
        }

        target
          .addGuardCheck(
            'StubGuard1',
            'check1',
            jsHelpers.getFunctionBody(check1)
          );

        try {
          assert.equal(
            true,
            target
              .runCheck(
                'StubGuard1',
                'check1',
                {
                  fold: 'fold1',
                  plugin: 'StubPlugin1',
                  method: 'echo'
                }
              )
          );

          done();
        } catch (e) {
          done(e);
        }
      });

      it('does not check', function (done) {
        guardSocket = new EventEmitter();

        target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
        target.addGuard('StubGuard1', guardSocket);

        function check1(reject) {
          if (fold === 'fold1' && plugin === 'StubPlugin1' && method === 'echo') return;

          reject();
        }

        target
          .addGuardCheck(
            'StubGuard1',
            'check1',
            jsHelpers.getFunctionBody(check1)
          );

        try {
          assert.equal(
            false,
            target
              .runCheck(
                'StubGuard1',
                'check1',
                {
                  fold: 'fold2',
                  plugin: 'StubPlugin1',
                  method: 'echo'
                }
              )
          );

          done();
        } catch (e) {
          done(e);
        }
      });

      it('runs checks on .invokeMethod', function (done) {
        var target2 = new Stack(
          testData.stack.privateKey,
          testData.stack.publicKey
        );
        var pluginSocket = new EventEmitter();

        pluginSocket
          .on(
            'api',
            function (token, context, methodName, params, callback) {
              callback(null, 'this');
            }
          );

        var guardSocket = new EventEmitter();
        var check1 = function (reject) {
          if (plugin === 'StubPlugin1' &&
            method === 'echo' &&
            fold === 'fold1') return;

          reject();
        };

        target2.addPlugin('StubPlugin1', pluginSocket, {
          'echo': ['what']
        });
        target2.authorizeGuardPublicKey('guard1', testData.guard1.publicKey);
        target2.addGuard('guard1', guardSocket);
        target2.addGuardCheck('guard1', 'check1', jsHelpers.getFunctionBody(check1));

        target2
          .invokeMethod(
            stackToken,
            {
              fold: 'fold1'
            },
            'StubPlugin1',
            'echo',
            {
              'what': 'this'
            }
          )
          .then(function (result) {
            try {
              assert.equal('this', result);

              target2
                .invokeMethod(
                  stackToken,
                  {
                    fold: 'fold2'
                  },
                  'StubPlugin1',
                  'echo',
                  {
                    'what': 'this'
                  }
                )
                .catch(function (error) {
                  assert.equal('not authorized', error);

                  done();
                });
            } catch (e) {
              done(e);
            }
          });
      });
    });
  });

  describe('.addGuard', function () {
    var guardSocket;

    beforeEach(function () {
      guardSocket = new EventEmitter();
    });

    it('adds guard socket', function () {
      target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
      target.addGuard('StubGuard1', guardSocket);

      assert.equal(true, target.isGuardConnected('StubGuard1'));
    });

    it('listens add-check', function () {
      var check1 = (function (reject) {
        if (fold === 'fold2') return reject();
      });

      var code = jsHelpers.getFunctionBody(check1);
      var signed = crypto.encryptAndSign(
        {
          code: code,
          name: 'check1'
        },
        testData.guard1.privateKey,
        testData.stack.publicKey
      );

      guardSocket.emit('add-check', signed, function () {
        assert.deepEqual(
          {
            'StubGuard1': ['check1']
          },
          target.listGuardChecks()
        );
      });
    });

    it('rejects add-check if badly signed', function () {
      var check2 = (function (reject) {
        if (fold === 'fold2') return reject();
      });

      var code = jsHelpers.getFunctionBody(check2);
      var signed = crypto.encryptAndSign(
        {
          code: code,
          name: 'check2'
        },
        testData.guard2.privateKey,
        testData.stack.publicKey
      );

      guardSocket
        .emit('add-check', signed, function (err) {
          assert.equal('invalid signature', err);

          assert.deepEqual(
            {
              'StubGuard1': ['check1']
            },
            target.listGuardChecks()
          );
        });
    });

    it('drops guard on disconnect', function () {
      guardSocket.emit('disconnect');

      assert.equal(false, target.isGuardConnected('StubGuard1'));
    });
  });

  describe('.addTokenProvider', function () {
    it('fails to add it if no socket is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            'StubTokenProvider1',
            null,
            { 'getToken': ['username'] },
            testData.stack.publicKey
          );
        },
        /socket is required/
      );
    });

    it('fails to add it if no name is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            null,
            new EventEmitter(),
            { 'getToken': ['username'] },
            testData.stack.publicKey
          );
        },
        /name is required/
      );
    });

    it('fails to add it if no methods is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            'StubTokenProvider1',
            new EventEmitter(),
            null,
            testData.stack.publicKey
          );
        },
        /methods are required/
      );
    });

    it('fails to add it if no public key is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            'StubTokenProvider1',
            new EventEmitter(),
            { 'getToken': ['username'] },
            null
          );
        },
        /public key is required/
      );
    });

    it('adds a token provider', function () {
      var stubSocket = new EventEmitter();

      target
        .addTokenProvider(
          'StubTokenProvider1',
          stubSocket,
          { 'getToken': ['username'] },
          testData.tokenProvider.publicKey
        );

      assert.deepEqual(
        {
          'StubTokenProvider1': { 'getToken': ['username'] }
        },
        target.listTokenProviders()
      );
    });

    it('verifies token', function () {
      target
        .addTokenProvider(
          'StubTokenProvider1',
          new EventEmitter(),
          {},
          testData.tokenProvider.publicKey
        );

      assert.deepEqual(
        {
          username: 'myuser'
        },
        target
          .verifyToken(
            'StubTokenProvider1',
            et2s({
              username: 'myuser'
            })
          )
      );
    });
  });

  describe('.getContextFromToken', function () {
    beforeEach(function () {
      target = new Stack(
        testData.stack.privateKey,
        testData.stack.publicKey
      );
    });

    it('decodes token context', function () {
      var context = target.getContextFromToken(stackToken);

      assert.deepEqual({
        'fold': 'fold1',
        'username': 'myuser'
      }, context);
    });
  });

  describe('.getToken', function () {
    beforeEach(function () {
      target = new Stack(
        testData.stack.privateKey,
        testData.stack.publicKey
      );
    });

    it('passes call to token provider', function (done) {
      var stubSocket = new EventEmitter();

      stubSocket
        .on(
          'get-token',
          function (
            stackToken,
            context,
            methodName,
            params,
            callback
          ) {
            try {
              assert.equal(null, stackToken);
              assert.deepEqual(
                {},
                context
              );
              assert.equal('getToken', methodName);

              assert.deepEqual(
                {
                  'username': 'user1'
                },
                params
              );

              assert(callback);

              done();
            } catch (e) {
              done(e);
            }
          }
        );

      target
        .addTokenProvider(
          'StubTokenProvider1',
          stubSocket,
          { 'getToken': ['username'] },
          testData.tokenProvider.publicKey
        );

      target
        .getToken(
          null,
          'StubTokenProvider1',
          'getToken',
          {
            'username': 'user1'
          }
        );
    });
  });

  describe('bus2bus', function () {
    var target;
    var pluginBus1;
    var pluginBus2;
    var pluginBus1be;
    var pluginBus2be;
    var tokenProviderBus;
    var clientBus;

    describe('client2plugin', function () {
      beforeEach(function () {
        target = new Stack(testData.stack.privateKey, testData.stack.publicKey);

        var pluginBus1CE = new ConnectedEmitters();
        var pluginBus2CE = new ConnectedEmitters();
        var tokenProviderBusCE = new ConnectedEmitters();
        var clientBusCE = new ConnectedEmitters();

        pluginBus1 = pluginBus1CE.createEmitter();
        pluginBus2 = pluginBus2CE.createEmitter();
        clientBus = clientBusCE.createEmitter();
        tokenProviderBus = tokenProviderBusCE.createEmitter();

        target.addPlugin('StubPlugin1', (pluginBus1be = pluginBus1CE.createEmitter()), { echo: ['what'] });
        target.addPlugin('StubPlugin2', (pluginBus2be = pluginBus2CE.createEmitter()), { echo: ['what'] });
        target.listenSocket(clientBusCE.createEmitter());
        target.addTokenProvider('TokenProvider1', tokenProviderBusCE.createEmitter(), {}, testData.tokenProvider.publicKey);
      });

      describe('form client', function () {
        describe('api', function () {
          it('passes call method', function (done) {
            pluginBus1
              .on(
                'api',
                function (token, context, methodName, params, callback) {
                  try {
                    assert.equal(token, stackToken);
                    assert.deepEqual({
                      fold: 'fold1',
                      username: 'myuser'
                    }, context);
                    assert.equal('echo', methodName);
                    assert.deepEqual({
                      'what': 'hello'
                    }, params);

                    done();
                  } catch (e) {
                    done(e);
                  }
                }
              );

            clientBus
              .emit(
                'api',
                stackToken,
                'StubPlugin1',
                'echo',
                { 'what': 'hello' },
                function (err, result) {
                }
              );
          });

          it('answers successful call', function (done) {
            pluginBus1
              .on(
                'api',
                function (
                  token, pluginName, methodName, params, callback) {
                  callback(null, params.what);
                }
              );

            clientBus
              .emit(
                'api',
                stackToken,
                'StubPlugin1',
                'echo',
                { 'what': 'hello' },
                function (err, result) {
                  try {
                    assert.equal('hello', result);

                    done();
                  } catch (e) {
                    done(e);
                  }
                }
              );
          });

          it('answers unsuccessful call', function (done) {
            pluginBus1
              .on(
                'api',
                function (
                  token,
                  context,
                  methodName,
                  params,
                  callback
                ) {
                  callback('my error');
                }
              );

            clientBus
              .emit(
                'api',
                stackToken,
                'StubPlugin1',
                'echo',
                { 'what': 'hello' },
                function (err, result) {
                  try {
                    assert.equal('my error', err);

                    done();
                  } catch (e) {
                    done(e);
                  }
                }
              );
          });

          it('rejects non-existent plugin', function (done) {
            clientBus
              .emit(
                'api',
                stackToken,
                'IDontExists',
                'echo',
                { 'what': 'hello' },
                function (err, result) {
                  try {
                    assert.equal('invalid plugin', err);

                    done();
                  } catch (e) {
                    done(e);
                  }
                }
              );
          });

          it('rejects non-existent method', function (done) {
            clientBus
              .emit(
                'api',
                stackToken,
                'StubPlugin1',
                'IDontExists',
                { 'what': 'hello' },
                function (err, result) {
                  try {
                    assert.equal('invalid method', err);

                    done();
                  } catch (e) {
                    done(e);
                  }
                }
              );
          });
        });

        describe('describe-apis', function () {
          it('returns existing plugins with methods', function (done) {
            clientBus
              .emit(
                'describe-apis',
                function (err, apis) {
                  assert.deepEqual({
                    'StubPlugin1': {
                      echo: ['what']
                    },
                    'StubPlugin2': {
                      echo: ['what']
                    }
                  }, apis);

                  done();
                }
              );
          });
        });

        describe('describe-api', function () {
          it('returns method for existing plugin', function (done) {
            clientBus
              .emit(
                'describe-api',
                'StubPlugin1',
                function (err, methods) {
                  assert.deepEqual({
                    echo: ['what']
                  }, methods);

                  done();
                }
              );
          });

          it('returns Plugin unavailable for non-existent plugins', function (done) {
            clientBus.emit(
              'describe-api',
              'DoesNotExists',
              function (err, methods) {
                try {
                  assert.equal('Plugin unavailable', err);

                  done();
                } catch (e) {
                  done(e);
                }
              }
            );
          });
        });
      });
    });
  });

  describe('round-robing', function () {
    var socket1;
    var socket2;

    it('invokes in a round-robin way', function () {
      var invoked = [];
      socket1 = new EventEmitter();
      socket2 = new EventEmitter();
      target.addPlugin('Plugin1', socket1, { method1: [] });
      target.addPlugin('Plugin1', socket2, { method1: [] });

      socket1
        .on('api', function () {
          invoked.push(1);
        });
      socket2
        .on('api', function () {
          invoked.push(2);
        });

      target.invokeMethod(null, {}, 'Plugin1', 'method1', {})
        .catch(() => {
          // YOLO: we know this will fail
        });
      target.invokeMethod(null, {}, 'Plugin1', 'method1', {})
        .catch(() => {
          // YOLO: we know this will fail
        });
      target.invokeMethod(null, {}, 'Plugin1', 'method1', {})
        .catch(() => {
          // YOLO: we know this will fail
        });
      target.invokeMethod(null, {}, 'Plugin1', 'method1', {})
        .catch(() => {
          // YOLO: we know this will fail
        });

      assert.deepEqual(
        invoked,
        [2, 1, 2, 1]
      );
    });
  });
})