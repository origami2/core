var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var PenelopeTokenProvider = require('../penelope-token-provider');

describe('PenelopeTokenProvider', function () {
    it('exports a function', function () {
        assert.equal(typeof (PenelopeTokenProvider), 'function');
    });

    it('requires a stack', function () {
        assert.throws(
            function () {
                new PenelopeTokenProvider();
            },
            /plugin is required/
        );
    });

    context('with a plugin', () => {
        let plugin = {};

        beforeEach(() => {
            plugin = {};
        });

        it('returns a function', () => {
            let tokenProvieder = new PenelopeTokenProvider(plugin);

            assert(typeof (tokenProvieder) === 'function');
        });

        context('with a socket', () => {
            let socket;
            let tokenProvider;
            let locals;
            let dependencies;

            beforeEach(() => {
                socket = new EventEmitter();
                tokenProvider = new PenelopeTokenProvider(plugin, locals, dependencies);
            });

            it('requires socket', () => {
                assert.throws(() => tokenProvider(), /socket is required/);
            });

            it('passes through describe-methods from socket', (done) => {
                const describeMethodsResult = { methods: ['abc'] };

                plugin.describeMethods = () => describeMethodsResult;

                tokenProvider(socket);

                socket.emit('describe-methods', (err, result) => {
                    try {
                        assert.equal(err, undefined);
                        assert.equal(result, describeMethodsResult);

                        done();
                    } catch (e) {
                        done(e);
                    }
                });
            });

            context('without dependencies', () => {
                beforeEach(() => {
                    for (let key in dependencies) delete dependencies[key];
                })

                describe('#get-token', () => {
                    it('invokes plugin .invokeMethod', (done) => {
                        let stackToken = undefined;
                        let context = {};
                        let methodName = 'the-method';
                        let methodParams = {
                            param1: 'p1'
                        };
                        let callback = (err, result) => { };

                        plugin.invokeMethod = (
                            invokedContext,
                            invokedMethodName,
                            invokedMethodParams,
                            invokedCallback) => {
                            try {
                                assert.equal(context, invokedContext);
                                assert.equal(methodName, invokedMethodName);
                                assert.equal(methodParams, invokedMethodParams);
                                assert.equal(callback, invokedCallback);

                                done();
                            } catch (e) {
                                done(e);
                            }
                        };

                        tokenProvider(socket);

                        socket.emit(
                            'get-token',
                            stackToken,
                            context,
                            methodName,
                            methodParams,
                            callback);
                    });

                    it('returns new token', (done) => {
                        let stackToken = undefined;
                        let context = {};
                        let methodName = 'the-method';
                        let methodParams = {
                            param1: 'p1'
                        };
                        let newToken = { user: 'fake' };

                        let callback = (err, result) => {
                            try {
                                assert.equal(err, null);
                                assert.equal(result, newToken);

                                done();
                            } catch (e) {
                                done(e);
                            }
                        };

                        plugin.invokeMethod = (
                            invokedContext,
                            invokedMethodName,
                            invokedMethodParams,
                            invokedCallback) => {
                            callback(null, newToken);
                        };

                        tokenProvider(socket);

                        socket.emit(
                            'get-token',
                            stackToken,
                            context,
                            methodName,
                            methodParams,
                            callback);
                    });
                });
            });

            context('with dependencies', () => {
                beforeEach(() => {
                    dependencies = {
                        'dep1': 'dep1'
                    };
                    socket = new EventEmitter();
                    tokenProvider = new PenelopeTokenProvider(plugin, locals, dependencies);
                });

                describe('#get-token', () => {
                    it('invokes socket #describe-apis', (done) => {
                        let stackToken = undefined;
                        let context = {};
                        let methodName = 'the-method';
                        let methodParams = {
                            param1: 'p1'
                        };
                        let callback = (err, result) => {
                            debugger;
                        };

                        socket.on('describe-apis', (callback) => {
                            try {
                                assert.equal(typeof (callback), 'function');

                                done();
                            } catch (e) {
                                done(e);
                            }
                        });

                        tokenProvider(socket);

                        socket.emit(
                            'get-token',
                            stackToken,
                            context,
                            methodName,
                            methodParams,
                            callback);
                    });

                    context('when describe-apis fails', (done) => {
                        it('calls back with error', (done) => {
                            let stackToken = undefined;
                            let context = {};
                            let methodName = 'the-method';
                            let methodParams = {
                                param1: 'p1'
                            };
                            let theError = 'some error';

                            let callback = (err, result) => {
                                try {
                                    assert.equal(err, theError);

                                    done();
                                } catch (e) {
                                    done(e);
                                }
                            };

                            socket.on('describe-apis', (callback) => {
                                callback(theError);
                            });

                            tokenProvider(socket);

                            socket.emit(
                                'get-token',
                                stackToken,
                                context,
                                methodName,
                                methodParams,
                                callback);
                        });
                    });

                    context('when describe-apis succeds', (done) => {
                        it('invokes method with context', (done) => {
                            let stackToken = undefined;
                            let context = {};
                            let methodName = 'the-method';
                            let methodParams = {
                                param1: 'p1'
                            };

                            let theApis = {
                                'dep1': {
                                    'method1': ['param1']
                                }
                            };

                            socket.on('describe-apis', (callback) => {
                                callback(null, theApis);
                            });

                            plugin.invokeMethod = (
                                invokedContext,
                                invokedMethod,
                                invokedMethodParams) => {
                                try {
                                    assert(invokedContext.dep1);
                                    assert.equal(methodName, invokedMethod);
                                    assert.equal(methodParams, invokedMethodParams);

                                    done();
                                } catch (e) {
                                    done(e);
                                }
                            };

                            tokenProvider(socket);

                            socket.emit(
                                'get-token',
                                stackToken,
                                context,
                                methodName,
                                methodParams,
                                () => { });
                        });
                    });

                    it('returns new token', (done) => {
                        let stackToken = undefined;
                        let context = {};
                        let methodName = 'the-method';
                        let methodParams = {
                            param1: 'p1'
                        };

                        let theApis = {
                            'dep1': {
                                'method1': ['param1']
                            }
                        };

                        socket.on('describe-apis', (callback) => {
                            callback(null, theApis);
                        });
                        let theResult = { user: 'fake' };

                        plugin.invokeMethod = (
                            invokedContext,
                            invokedMethod,
                            invokedMethodParams) => Promise.resolve(theResult);

                        tokenProvider(socket);

                        socket.emit(
                            'get-token',
                            stackToken,
                            context,
                            methodName,
                            methodParams,
                            (err, result) => {
                                try {
                                    assert.equal(err, null);
                                    assert.equal(theResult, result);

                                    done();
                                } catch (e) {
                                    done(e);
                                }
                            });
                    });
                    
                    it('returns error', (done) => {
                        let stackToken = undefined;
                        let context = {};
                        let methodName = 'the-method';
                        let methodParams = {
                            param1: 'p1'
                        };

                        let theApis = {
                            'dep1': {
                                'method1': ['param1']
                            }
                        };

                        socket.on('describe-apis', (callback) => {
                            callback(null, theApis);
                        });

                        let theError = 'some error';

                        plugin.invokeMethod = (
                            invokedContext,
                            invokedMethod,
                            invokedMethodParams) => Promise.reject(theError);

                        tokenProvider(socket);

                        socket.emit(
                            'get-token',
                            stackToken,
                            context,
                            methodName,
                            methodParams,
                            (err, result) => {
                                try {
                                    assert.equal(err, theError);

                                    done();
                                } catch (e) {
                                    done(e);
                                }
                            });
                    });
                });
            });
        });
    });
});