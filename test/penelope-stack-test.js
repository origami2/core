var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var PenelopeStack = require('../penelope-stack');

describe('PenelopeStack', function () {
  it('exports a function', function () {
    assert.equal(typeof (PenelopeStack), 'function');
  });

  it('requires a stack', function () {
    assert.throws(
      function () {
        new PenelopeStack();
      },
      /stack is required/
    );
  });

  it('returns a function', function () {
    var target = new PenelopeStack({});

    assert.equal(typeof (target), 'function');
  });

  it('requires a socket', function () {
    var target = new PenelopeStack({});

    assert.throws(
      function () {
        target();
      },
      /socket is required/
    );
  });

  it('requires params', function () {
    var target = new PenelopeStack({});

    assert.throws(
      function () {
        target(new EventEmitter());
      },
      /params are required/
    );
  });

  it('resolves promise', function (done) {
    var target = new PenelopeStack({});

    target(new EventEmitter(), { namespace: 'Events' })
      .then(function () {
        done();
      });
  });

  context('once connected', () => {
    it('resolves promise', function (done) {
      var target = new PenelopeStack({});

      target(new EventEmitter(), { namespace: 'Events' })
        .then(function () {
          done();
        });
    });
  });

  it('returns a promise', function () {
    var target = new PenelopeStack({});

    var socket = new EventEmitter();

    var r = target(socket, { namespace: 'Events' });
    assert(r instanceof Promise);
  });
});