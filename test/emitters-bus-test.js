var assert = require('assert');
var EmittersBus = require('../emitters-bus');

describe('EmittersBus', function () {
  it('instantiates', function () {
    new EmittersBus();
  });
  
  describe('.createEmitter', function () {
    var connected;
    
    beforeEach(function () {
      connected = new EmittersBus();
    });
    
    var emitter1;
    
    beforeEach(function () {
      emitter1 = connected.createEmitter();
    });
    
    it('returns an object', function () {
      assert(emitter1);
    });
    
    var emitter2;
    
    beforeEach(function () {
      emitter2 = connected.createEmitter();
    });
    
    it('what is emitted in one emitter is listened in the other', function (done) {
      emitter2.on('a', done);
      emitter1.emit('a');
    });
    
    it('same functionality with addEventListener', function (done) {
      emitter2.addEventListener('a', done);
      emitter1.emit('a');
    });
    
    it('what is emitted is not emitted on itself', function (done) {
      emitter1.on('b', function () {
        try {
          assert.fail('this shouldnt\'t have been called');
        } catch (e) {
          done(e);
        }
      });
      
      emitter1.emit('b');
      
      done();
    });
    
    it('you can listen any event also', function (done) {
      emitter2.onAny(function (eventName) {
        try {
          assert.equal('c', eventName);
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      emitter1.emit('c');
    });
    
    it('you can listen any event and remove listener', function () {
      var listener = function (eventName) {
        throw new Error('this should\nt have been reached');
      };
      
      emitter2.onAny(listener);
      emitter2.offAny(listener);
      
      emitter1.emit('d');
    });
    
    it('you can remove an specific listener with removeEventListener', function () {
      var listener = function (eventName) {
        throw new Error('this should\nt have been reached');
      };
      
      emitter2.on('e', listener);
      emitter2.removeListener('e', listener);
      
      emitter1.emit('e');
    });
        
    it('you can remove an specific listener with removeAllListeners', function () {
      var listener = function (eventName) {
        throw new Error('this should\nt have been reached');
      };
      
      emitter2.on('e', listener);
      emitter2.removeAllListeners('e');
      
      emitter1.emit('e');
    });
  });
})