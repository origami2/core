var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var PenelopeStackTokenProvider = require('../penelope-stack/token-provider');

describe('PenelopeStackTokenProvider', function () {
    var fakeStack, fakeSocket, fakePublicKey;

    beforeEach(function () {
        fakeStack = new EventEmitter();
        fakeSocket = new EventEmitter();
        fakePublicKey = 'fakePublicKey';
    });

    it('exports a function', function () {
        assert.equal(typeof (PenelopeStackTokenProvider), 'function');
    });

    it('requires a stack', function () {
        assert.throws(
            function () {
                new PenelopeStackTokenProvider();
            },
            /stack is required/
        );
    });

    it('requires a socket', function () {
        var target = new PenelopeStackTokenProvider(fakeStack);

        assert.throws(
            function () {
                target();
            },
            /socket is required/
        );
    });

    it('requires params', function () {
        var target = new PenelopeStackTokenProvider(fakeStack);

        assert.throws(
            function () {
                target(fakeSocket);
            },
            /params are required/
        );
    });

    it('requires name param', function () {
        var target = new PenelopeStackTokenProvider(fakeStack);

        assert.throws(
            function () {
                target(fakeSocket, {});
            },
            /name is required/
        );
    });

    it('requires methods param', function () {
        var target = new PenelopeStackTokenProvider(fakeStack);

        assert.throws(
            function () {
                target(fakeSocket, { name: {} });
            },
            /methods are required/
        );
    });


    it('requires methods param', function () {
        var target = new PenelopeStackTokenProvider(fakeStack);

        assert.throws(
            function () {
                target(fakeSocket, { name: {}, methods: {} });
            },
            /public key is required/
        );
    });

    context('returning a promise', function () {
        it('when .addTokenProvider works', (done) => {
            var target = new PenelopeStackTokenProvider(fakeStack);

            var fakeSocket = new EventEmitter();

            fakeStack.addTokenProvider = (pluginName, socket, methods, publicKey) => {
                try {
                    assert.equal(pluginName, 'Plugin1');
                    assert.equal(socket, fakeSocket);
                    assert.deepEqual(methods, { 'method1': ['p1'] });
                    assert.deepEqual(publicKey, fakePublicKey);

                    done();
                } catch (e) {
                    done(e);
                }
            };

            var r = target(fakeSocket, { 'name': 'Plugin1', 'methods': { 'method1': ['p1'] }, 'publicKey': fakePublicKey });
            assert(r instanceof Promise);
        });

        it('resolves with result', (done) => {
            var target = new PenelopeStackTokenProvider(fakeStack);

            var fakeSocket = new EventEmitter();

            fakeStack.addTokenProvider = (pluginName, socket, methods, publicKey) => {
            };

            var r = target(fakeSocket, { 'name': 'Plugin1', 'methods': { 'method1': ['p1'] }, 'publicKey': fakePublicKey });
            assert(r instanceof Promise);

            r.then(() => done());
        });

        it('when .addTokenProvider fails', (done) => {
            var target = new PenelopeStackTokenProvider(fakeStack);

            var fakeSocket = new EventEmitter();
            let theError = 'some error';

            fakeStack.addTokenProvider = (pluginName, socket, methods, publicKey) => {
                throw new Error(theError);
            };

            var r = target(fakeSocket, { 'name': 'Plugin1', 'methods': { 'method1': ['p1'] }, 'publicKey': fakePublicKey });
            assert(r instanceof Promise);

            r.catch((err) => {
                try {
                    assert.throws(() => {
                        throw err;
                    }, /some error/);

                    done();
                } catch (e) {
                    done(e);
                }
            })
        });

        it('when .addTokenProvider succeds', (done) => {
            var target = new PenelopeStackTokenProvider(fakeStack);

            var fakeSocket = new EventEmitter();
            let theError = 'some error';

            fakeStack.addTokenProvider = (pluginName, socket, methods, publicKey) => { };

            var r = target(fakeSocket, { 'name': 'Plugin1', 'methods': { 'method1': ['p1'] }, 'publicKey': fakePublicKey });
            assert(r instanceof Promise);

            r.then(() => done())
        });
    });
});

