/* global context */

var NameRegistry = require('../name-registry');
var assert = require('assert');
var crypto = require('../crypto-utils');
var testData = {
  "pair1": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqgeAsH\nZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jkbFZJ\nJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQKKx5\nVFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjOkSB8\ncUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdix0/H\nBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey":"-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqg\neAsHZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jk\nbFZJJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQ\nKKx5VFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjO\nkSB8cUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdi\nx0/HBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQABAoIBAG0m0AHi5HyJ5SbU\nxNJ46LTBDAX3DkoNkQgRsUGAQV6sMOKVbEYa0SI5wyZEKfuGVaZYUv5mDBqL/ka9\nYkkaatqj/97TvMOoyt8tH8nIowuXyF7xNSH4LeEUZLqDr9VkYJhk38RPDMuc99fp\npsEZsTpmC56ygRZS/cVObZjefoKkl+2cPL0jfSWUSn2t/nUKbmwNbjbM6f1OCCyJ\n814LZalfeLh+hfEdY8yVK0Rlaut/FLw4eaY43H0QTwDyYXmgOgYckia7MQmtu5yO\n0dFRDZa7x0j01w0+Hphr88bhtXFgv8VPWyzyz2d8fNWlbMP4JCARes3lVpkMKHDn\nFZIULcECgYEA53qsuOQrE3xHtNzfak7FjGgkQTxi4TeYXRJ5Zhlkjtb9nQnsFzTq\n+AXpeHfsGmaza9/LcMUJ9JzYV7t5V8T6vZT3riOnjQAfbHOrR3EYK5/vv4ZfjFgk\nBySoNjOtf67ZXn1lRJ3FZNN4RfGmh/2aqYi5PAn1g5+nLXtMIY88a1ECgYEAxGp9\nW03pH4QwBzvFA9PJ+BhWXS8fAcafY67TlXg+IxzGP/YQupodovDChXpvbfYV0kf6\nU6YddDQT5p0j2YmssJCaDa93iMXU2/JgI5pcTNFxLa5LR/Mg30JYyniK/4+YIkoL\n68V2OH0Y65DIQdajHPojzFBmDSBZSnkyO9Ojx2MCgYADCoSMZUr/lYlnoeM5hVFp\nF9EqHj36UX2p810u7zR3//ETCBdW8rYHjiRUFdc/PYwr5aPJln0b/peFB4x/j7Hv\nna5nVkaUPqUrCpX8eUrk/9Ppgz1sHZhTk7K2C5XC8KwgZqtW7G+0dGbHHHagoL9Q\nbOBqHoNgOE+89Dq60iPsEQKBgByahXbueaylS3lCMwbDqP4ATVN0sUdI7Z1OsHFr\n+WCTqCtYYkdKelZoSWu20NNqqvLcmI/l+RQbIWrMJ5RegE+WP1kO3JGGfeEqAuYs\nbJSjS6AjacMonPjmaJfTxipBdx5HOkUzlGvVi/OCOiecYlSt+NigPLxcoaQ+0hn0\nUD2RAoGAUH2QV31iSFLs+ByQdGmPaYuVxhNUwaGoN0M5splIdH6DAgyIvM5ogXVb\nMV3yUdnULU+5YeL8r93NeFRHiBCBfvcIcDdJHxRJNQFvdsAWosGeXI6clC1YkUnB\n2N+9PrizN87XUb6NmY07NwtAKrU6aZ0XwKUEgor9rntT8IawFo4=\n-----END RSA PRIVATE KEY-----"
  },
  "pair2": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4\nPKtgrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQae\nsmwcu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIi\nGaR/A/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTE\nS+NTkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylh\nX2+xsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQABAoIBAQCzPkfPKGiXj/ho\npviEbfmfR9W2w0F+1V03Wk1Rjo8whxH6Rg9DuJpJcDOza95W2B+uz0Ds64Z6av0j\nmiK8SU4T8S8Yl8merXzHKCdq0IUBzRRan2s/48dsaRhn+G+wSs5yvQWAz0eXHGhR\n2xaPvXSTZdqnPzF/mrgUC3mp538a3TuqNzJYfowY4d+Rn6+8K5EqcWHHBBINBp90\nhWPxGMp2mTFDsycQaQjDsfGJDAlFtQJ0FlrU0KAHt5KhaX7jMvOvD7AuL8aEnnWy\n9yTwb+POpPxrz1a+6p4U7YIPef3yVmWS+QUI2aDtgz7LMlxyTfkX4OiwmBGw51ED\niIwKCYwBAoGBAOfD1WkLhjqAqPDYdm1VkRYQILDWv6vUhznevw/J8yItnpYakRe7\njmhdiQh/p3ieOaA/l31p2ZR4OGT2hZwefAFYbwVoCOAtoIelbJwz5Mu1H7mMBuav\n23/K41edQ25zRbEjy1f20CMBHbEhF+64ufRGUUnJcA0gV9dO4ZvdvQ0jAoGBAMju\nwM/T0sTM83l1IPRvd7moTKzhOt2dJXq660PVY1FqLKSvXicCsFQtEd4J2K2Aik+n\n7A9m0DJx/QauPZ3Dk2K1dWCXqTRp7Yjhvpmw2mvrTfRNFww+Ths15XDCAtRWzUSV\n8yB9TnTH8opeUejWcoI8VikhVtkR8hF5m0xhGXEZAoGAYe4Rwu0nrpimyf3tLI23\nXIc8CPz0yHppGT7RpK5EmfCEzhAztr99kQPU305xSToyR1AXhtqvIVkbGy4/jpQi\n+b6QSsyG05Lz/d0cY3RZ/OlvmktcryUnrnvgTCkbURRMImlphqW3lNLn5OyC3FAZ\n4unDd1YyjplYBJZEJkQvmdkCgYBbtV0bfjq8yC54SRV5e0bR3hbg8QvFCmyIz7eB\nhmuGRUeLAt+ePoPsZ4W4KhZTWk/Ge4YqoWp1G8G4wt4sm48xhlDEfXQlEBPyXdZ4\nn1eR2hwCXQ0f8XAEy1ylUmeoMtYNb8NAGPEuK/RvYw1PkFFDT/ajXywcdyxc/Xv8\nKHVMQQKBgFPnGNRfLtCVfONwcveqfUeR4xS4UJkTNjLExTzBRKoUuKLbZwV5tHEr\nBFoMaM3V5pPLKfImO4BYZeuPdB1DBG3dsxL5G5TzWBauJq7OGAQmXuHecV4pblHm\nPO9yEktLnn8QAkbiePv+PhFFnSAa93cYHNtM68bBTdLrohBzZbDW\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4PKtg\nrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQaesmwc\nu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIiGaR/\nA/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTES+NT\nkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylhX2+x\nsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQAB\n-----END RSA PUBLIC KEY-----"
  }
};

describe('NameRegistry', function () {
  describe('constructor', function () {
    it('requires private key', function () {
      assert.throws(
        function () {
          new NameRegistry();
        },
        /private key is required/
      );
    });

    it('accepts private key', function () {
      new NameRegistry(testData.pair1.privateKey);
    });
  });

  describe('.listKnownNamespaces', function () {
    it('lists no namespaces at first', function () {

      assert.deepEqual(
        [
        ],
        new NameRegistry(testData.pair1.privateKey).listKnownNamespaces()
      );
    });
  });

  describe('.getPublicKey', function () {
    it('returns public key', function () {
      var target = new NameRegistry(testData.pair1.privateKey);

      var publicKey = target.getPublicKey();

      assert.equal(publicKey, testData.pair1.publicKey);
    });
  });

  describe('.addPublicNamespace', function () {
    var target;
    
    beforeEach(function () {
      target = new NameRegistry(testData.pair1.privateKey);
    });
    
    it('requires namespace name', function () {
      assert
      .throws(
        function () {
          target.addPublicNamespace();
        },
        /namespace is required/
      );
    });
    
    it('answer false to .isKnownNamespace', function () {
      assert.equal(false, target.isKnownNamespace('n1'));
    });
    
    it('accepts namespace', function () {
      target.addPublicNamespace('n1');
    });
    
    it('answer true to .isKnownNamespace', function () {
      target.addPublicNamespace('n1');
      
      assert.equal(true, target.isKnownNamespace('n1'));
    });
    
    it('lists it in .listKnownNamespaces', function () {
      target.addPublicNamespace('n1');
      
      assert.deepEqual(
        [
          'n1'
        ],
        target.listKnownNamespaces()
      );
    });
  });

  describe('.authorizeNamespace', function () {
    it('requires namespace', function () {
      var target = new NameRegistry(testData.pair1.privateKey);

      assert.throws(
        function () {
          target.authorizeNamespace();
        },
        /namespace is required/
      );
    });

    it('requires public key', function () {
      var target = new NameRegistry(testData.pair1.privateKey);

      assert.throws(
        function () {
          target.authorizeNamespace('ns1');
        },
        /public key is required/
      );
    });

    describe('with valid namespace and public key', function () {
      var target;

      before(function () {
        target = new NameRegistry(testData.pair1.privateKey);
      });

      it('works silently', function () {
        target.authorizeNamespace('ns1', testData.pair2.publicKey);
      });

      it('lists namespace in listKnownNamespaces', function () {
        target.authorizeNamespace('ns1', testData.pair2.publicKey);
        
        assert.deepEqual(
          [
            'ns1'
          ],
          target.listKnownNamespaces()
        );
      });

      it('lists namespace in isKnownNamespace', function () {
        target.authorizeNamespace('ns1', testData.pair2.publicKey);
        
        assert.equal(
          true,
          target.isKnownNamespace('ns1')
        );
      });
    });
    
    describe('.isKnownNamespace', function () {
      it('returns false on unknown namespace', function () {
        var target = new NameRegistry(testData.pair1.privateKey);
        
        assert.equal(false, target.isKnownNamespace('ns1'));
      });
    });

    describe('.answerChallenge', function () {
      var target;

      before(function () {
        target = new NameRegistry(testData.pair1.privateKey);
      });

      it('requires message', function () {
        assert.throws(
          function () {
            target.answerChallenge();
          },
          /message is required/
        );
      });

      it('requires message.code', function () {
        assert.throws(
          function () {
            target.answerChallenge({
              namespace: 'ns2',
              encryptFor: testData.pair2.publicKey
            });
          },
          /message.code is required/
        );
      });

      it('requires message.namespace', function () {
        assert.throws(
          function () {
            target.answerChallenge({
              code: 'abcd1234',
              encryptFor: testData.pair2.publicKey
            });
          },
          /message.namespace is required/
        );
      });

      it('requires message.encryptFor', function () {
        assert.throws(
          function () {
            target.answerChallenge({
              code: 'abcd1234',
              namespace: 'ns2'
            });
          },
          /message.encryptFor is required/
        );
      });

      it('encrypts and signs answer', function () {
        var response = target.answerChallenge({
          code: 'abcd1234',
          encryptFor: testData.pair2.publicKey,
          namespace: 'ns2'
        });

        assert(response);
        assert(response.data);
        assert.equal(testData.pair1.publicKey, response.signedBy);

        var decrypted = crypto.decryptAndVerify(
          response.data,
          testData.pair2.privateKey,
          testData.pair1.publicKey
        );

        assert.deepEqual(
          [
            'abcd1234',
            'ns2'
          ],
          decrypted
        );
      });
    });
  });

  describe('.createChallenge', function () {
    var target;

    before(function () {
      target = new NameRegistry(testData.pair1.privateKey);
    });

    it('requires a namespace', function () {
      assert.throws(
        function () {
          target.createChallenge();
        },
        /namespace is required/
      );
    });

    it('returns challenge with namespace, code and token', function () {
      var result = target.createChallenge('ns2');

      assert(result);
      assert(result.code);
      assert.equal(result.namespace, 'ns2');
      assert.equal(result.encryptFor, testData.pair1.publicKey);
    });
  });

  describe('.verifyChallenge', function () {
    var target;
    var sampleMessage;
    var sampleResponse;

    before(function () {
      target = new NameRegistry(testData.pair1.privateKey);
      target.authorizeNamespace('ns2', testData.pair2.publicKey);

      var target2 = new NameRegistry(testData.pair2.privateKey);

      sampleMessage = target.createChallenge('ns2');
      sampleResponse = target2.answerChallenge(sampleMessage);
    });

    it('requires message', function () {
      assert.throws(
        function () {
          target.verifyChallenge();
        },
        /message is required/
      );
    });

    it('requires message.code', function () {
      assert.throws(
        function () {
          target.verifyChallenge({
            namespace: 'ns2',
            encryptFor: testData.pair1.publicKey
          });
        },
        /message.code is required/
      );
    });

    it('requires message.namespace', function () {
      assert.throws(
        function () {
          target.verifyChallenge({
            code: 'abcd1234',
            encryptFor: testData.pair1.publicKey
          });
        },
        /message.namespace is required/
      );
    });

    it('requires response', function () {
      assert.throws(
        function () {
          target.verifyChallenge({
            code: 'abcd1234',
            namespace: 'ns2',
            encryptFor: testData.pair1.publicKey
          });
        },
        /response is required/
      );
    });

    it('returns false on invalid response', function () {
      assert.equal(
        false,
        target.verifyChallenge(
          {
            code: 'abcd1234',
            namespace: 'ns2',
            encryptFor: testData.pair1.publicKey
          },
          {
          }
        )
      );
    });

    it('returns namespace on valid response', function () {
      assert.equal(
        'ns2',
        target.verifyChallenge(
          sampleMessage,
          sampleResponse
        )
      );
    });
  });
  
  describe(
    '.getNamespacePublicKey',
    function () {
      context('private namespace', function () {
        context('known', function () {
          it('returns nothing', function () {
            var target = new NameRegistry(testData.pair1.privateKey);
            
            target.addPublicNamespace('n1');
            
            assert(!target.getNamespacePublicKey('n1'));
          });
        });
      });
      
      context('private namespace', function () {
        context('known', function () {
          it('returns public key', function () {
            var target = new NameRegistry(testData.pair1.privateKey);
            
            target.authorizeNamespace('n1', testData.pair2.publicKey);
            
            assert.equal(testData.pair2.publicKey, target.getNamespacePublicKey('n1'));
          });
        });
      
        context('unknown', function () {
          it('throws error', function () {
            var target = new NameRegistry(testData.pair1.privateKey);
            
            assert.throws(
              function () {
                target.getNamespacePublicKey('n1');
              },
              /unknown namespace/
            );
          });
        });
      });
    }
  );
  
  describe('NameRegistry to NameRegistry', function () {
    it('challenges', function () {
      var nr1 = new NameRegistry(testData.pair1.privateKey);
      var nr2 = new NameRegistry(testData.pair2.privateKey);
      
      nr1.authorizeNamespace('n1', testData.pair2.publicKey);
      
      var message = nr1.createChallenge('n1');
      
      var response = nr2.answerChallenge(message);
      
      assert.equal('n1', nr1.verifyChallenge(message, response));
    })
  })
});