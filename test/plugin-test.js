var Plugin = require('../plugin');
var assert = require('assert');
var crypto = require('../crypto-utils');
var testData = {
  "plugin": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPC\nmR31TJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS6\n7C08YAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF\n9ARu5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGVi\nPlhtpImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUI\nNoYL8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQABAoIBAQCFX9Mqbc/jDLGb\n92+Q2eqTtMEiafE7bmznX5voa+bVk863h8eRGWj2FnG8aL2Cfq0EZRb9BKxCrJtQ\nL8fibtmaoLSYfxA4BOicQKnSozEWOhU47mBtiLQd9cDHf852l/VDsSzDfcm6jj9+\nF0UGB1zzmEnB/Fkue0HOzNKBGmbGE1/RWo4+co3XYpq/YZdIQQadsV6G2ma1IApm\n5t9AmEzhjMJsVFTZqzWQpoHmJRJjfwqqugmt4gpBCuYMkVcuN0g1olOLobF+pITn\nNjVaYimiFDsjhZyM4c0MFUYB27TeG7RV+QigHM/DW9rTbvTXxnTl/O4mipUe4FsK\nTdsbu4kBAoGBAPwJeudZz0cYM7MPzYE0/ksA9OrJwbhg+BjUJQ7CtFijJp8Cxt3d\ncu6DC5GOke0dXe845DLwriYKqJbae0FIeQvHrFTRpC2gGI5CJ3nPrZ3jbJhQLnQA\nMoF7EbJdpMM0I1xZsZ1cOy7jDbQ6/5Za4RC59xyqZ8hIi8vn9JVAkurPAoGBAIrd\nHyQ0NuZsQCxqNKPvU+1QnpFaoy65yA1kLZ/JDghjTiplxmv0WMuf1B3rq09RvTKK\nCEc7S8CredzLDtgQF7KiENnMno9e8bcMAddC+UaKsV9Eoy4mXGe8vTnQsGvlMkZI\n27gkM1o+Sbsl45Ayr3eYcpJD6K+txSjJYO8B3GIzAoGAUQ5ndYIftHinH95kNDqr\n0clj+yKZ58df4vRPWrjpsVv/LsKA3Je8v9JrZQuaCM0aCbadRXi8OUXSRHnNjAhX\nzZ8Q4FJv37COVSoXcgiFiLK8mRuoZOwvUg8XeOq+83yQJsI96iLgccrZ/G3BB0UA\n/xUf0RtIt1QFibV2po2W8mcCgYBjLOXO35POIcX7cqbB5m3Ucd2uBkPBXWIpXkDP\ne7KP/wyWbzW1aD/6vd2quOQStFghvj+HUCwcINvZ+xRQ771dES5jvyYHU1Hi36p4\n6RZLcUaYudapYTBhzoR+xDMb/AdZ9zMlYoVikFXsWXUbSXfUPIanO+T1g2/qX1jh\nmjyhzQKBgQCvQdWSYMXaOnQRo/BHmRdR3X3aEZyLEn5UmXjxohUeB/Jtzb15FD/r\ny5ljkTVQyzYnRgwBuZBnUl/sCbxRp1CXbDmotHZ3DBwRJuP7KaPoj7dH+xfU6P3N\nuAm6CP+73AONmouW/8tVW60n2IkSH5I1ROYtPLrI6475M/hB4Anj+Q==\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPCmR31\nTJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS67C08\nYAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF9ARu\n5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGViPlht\npImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUINoYL\n8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQAB\n-----END RSA PUBLIC KEY-----"
  },
  "stack": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4\nPKtgrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQae\nsmwcu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIi\nGaR/A/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTE\nS+NTkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylh\nX2+xsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQABAoIBAQCzPkfPKGiXj/ho\npviEbfmfR9W2w0F+1V03Wk1Rjo8whxH6Rg9DuJpJcDOza95W2B+uz0Ds64Z6av0j\nmiK8SU4T8S8Yl8merXzHKCdq0IUBzRRan2s/48dsaRhn+G+wSs5yvQWAz0eXHGhR\n2xaPvXSTZdqnPzF/mrgUC3mp538a3TuqNzJYfowY4d+Rn6+8K5EqcWHHBBINBp90\nhWPxGMp2mTFDsycQaQjDsfGJDAlFtQJ0FlrU0KAHt5KhaX7jMvOvD7AuL8aEnnWy\n9yTwb+POpPxrz1a+6p4U7YIPef3yVmWS+QUI2aDtgz7LMlxyTfkX4OiwmBGw51ED\niIwKCYwBAoGBAOfD1WkLhjqAqPDYdm1VkRYQILDWv6vUhznevw/J8yItnpYakRe7\njmhdiQh/p3ieOaA/l31p2ZR4OGT2hZwefAFYbwVoCOAtoIelbJwz5Mu1H7mMBuav\n23/K41edQ25zRbEjy1f20CMBHbEhF+64ufRGUUnJcA0gV9dO4ZvdvQ0jAoGBAMju\nwM/T0sTM83l1IPRvd7moTKzhOt2dJXq660PVY1FqLKSvXicCsFQtEd4J2K2Aik+n\n7A9m0DJx/QauPZ3Dk2K1dWCXqTRp7Yjhvpmw2mvrTfRNFww+Ths15XDCAtRWzUSV\n8yB9TnTH8opeUejWcoI8VikhVtkR8hF5m0xhGXEZAoGAYe4Rwu0nrpimyf3tLI23\nXIc8CPz0yHppGT7RpK5EmfCEzhAztr99kQPU305xSToyR1AXhtqvIVkbGy4/jpQi\n+b6QSsyG05Lz/d0cY3RZ/OlvmktcryUnrnvgTCkbURRMImlphqW3lNLn5OyC3FAZ\n4unDd1YyjplYBJZEJkQvmdkCgYBbtV0bfjq8yC54SRV5e0bR3hbg8QvFCmyIz7eB\nhmuGRUeLAt+ePoPsZ4W4KhZTWk/Ge4YqoWp1G8G4wt4sm48xhlDEfXQlEBPyXdZ4\nn1eR2hwCXQ0f8XAEy1ylUmeoMtYNb8NAGPEuK/RvYw1PkFFDT/ajXywcdyxc/Xv8\nKHVMQQKBgFPnGNRfLtCVfONwcveqfUeR4xS4UJkTNjLExTzBRKoUuKLbZwV5tHEr\nBFoMaM3V5pPLKfImO4BYZeuPdB1DBG3dsxL5G5TzWBauJq7OGAQmXuHecV4pblHm\nPO9yEktLnn8QAkbiePv+PhFFnSAa93cYHNtM68bBTdLrohBzZbDW\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4PKtg\nrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQaesmwc\nu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIiGaR/\nA/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTES+NT\nkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylhX2+x\nsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQAB\n-----END RSA PUBLIC KEY-----"
  }
};

function StubPlugin (
  fold,
  constructorError,
  constructorStringError,
  methodError,
  methodStringError,
  local1,
  returnNoPromise
) {
  this.fold = fold;
  this.methodError = methodError;
  this.methodStringError = methodStringError;
  this.local1 = local1;
  this.returnNoPromise = returnNoPromise;

  if (constructorStringError) throw constructorStringError;
  if (constructorError) throw new Error('my constructor error');
}

StubPlugin.prototype.echo = function (what) {
  if (this.methodError) throw new Error('my method error');
  if (this.methodStringError) throw this.methodStringError;

  if (!this.fold) return Promise.reject('fold is required');

  if (this.returnNoPromise) return what;

  return Promise.resolve(what);
};

StubPlugin.prototype.echo2 = function (what) {
  return this.echo(what);
};

describe('Plugin', function () {
  var stackToken = crypto.encryptAndSign(
    {
      attr1: '1'
    },
    testData.stack.privateKey,
    testData.stack.publicKey
  );

  describe('constructor', function () {
    it('requires an API', function () {
      assert
        .throws(
          function () {
            new Plugin();
          },
          /API is required to be a function/
        );
    });

    it('instantiates', function () {
      var target = new Plugin(
        StubPlugin
      );

      assert(target);
    });
  });

  describe('.createInstance', function () {
    it('returns a promise', function () {
      var target = new Plugin(
        StubPlugin
      );

      var promise = target.createInstance(
        {
        }
      );

      assert(promise);
      assert(promise instanceof Promise);
    });

    it('creates an instance', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .createInstance(
        {
          fold: 'fold1'
        }
      )
      .then(function (instance) {
        try {
          assert('fold1', instance.fold1);

          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });

  it('fails promise on constructor error', function (done) {
    var target = new Plugin(
      StubPlugin
    );
    
    target
    .createInstance(
      {
        fold: 'fold1',
        constructorError: true
      }
    )
    .catch(function (err) {
      try {
        assert.equal(err, 'my constructor error');

        done();
      } catch (e) {
        done(e);
      }
    });
  });

  describe('.invokeMethod', function () {
    it('returns a promise', function () {
      var target = new Plugin(
        StubPlugin
      );

      var promise = target
      .invokeMethod(
        {
          fold: 'fold1'
        },
        'echo',
        {
          what: 'this'
        }
      );

      assert(promise);
      assert(promise instanceof Promise);
    });

    it('fails to method', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .invokeMethod(
        {
          fold: 'fold1',
          methodError: true
        },
        'echo',
        {
          what: 'this'
        }
      )
      .catch(
        (err) => {
          try {
            assert.deepEqual(
              'my method error',
              err
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });

    it('invokes method', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .invokeMethod(
        {
          fold: 'fold1'
        },
        'echo',
        {
          what: 'this'
        }
      )
      .then(
        function (result) {
          try {
            assert.deepEqual(
              'this',
              result
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });

    it('invokes method that returns no promise', (done) => {
      var target = new Plugin(
        StubPlugin
      );

      target
      .invokeMethod(
        {
          fold: 'fold1',
          returnNoPromise: true
        },
        'echo',
        {
          what: 'this'
        }
      )
      .then(
        function (result) {
          try {
            assert.deepEqual(
              'this',
              result
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });

    it('invokes method in the context of itself', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .invokeMethod(
        {
          fold: 'fold1'
        },
        'echo2',
        {
          what: 'this'
        }
      )
      .then(
        function (result) {
          try {
            assert.deepEqual(
              'this',
              result
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  });
  
  describe('.describeMethods', function () {
    it('returns methods with parameter names', function () {
      var target = new Plugin(
        StubPlugin
      );

      assert.deepEqual(
        {
          'echo': [ 'what' ],
          'echo2': [ 'what' ]
        },
        target.describeMethods()
      );
    });
  });
  
  describe('.getName', function () {
    it('returns plugin function name', function () {
      var target = new Plugin(
        StubPlugin
      );

      assert.equal(
        'StubPlugin',
        target.getName()
      );
    });
  });
});