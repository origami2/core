var Penelope = require('../penelope');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('Penelope', function () {
  it('requires a socket', function () {
    assert.throws(
      function () {
        new Penelope();
      },
      /socket is required/
    );
  });
  
  it('accepts socket', function () {
    var socket = new EventEmitter();
    new Penelope(socket);
  });
  
  describe('.addHandler', function () {
    var target;
    var socket;
    
    beforeEach(function () {
      socket = new EventEmitter();
      target = new Penelope(socket);
    });
    
    it('requires a kind', function () {
      assert.throws(
        function () {
          target.addHandler();
        },
        /kind is required/
      );
    });
    
    it('requires a handler', function () {
      assert.throws(
        function () {
          target.addHandler('kind1');
        },
        /handler is required/
      );
    });
    
    it('accepts handler', function () {
      target.addHandler('kind1', function () {});
    });
  });
  
  describe('.openSubsocket',function () {
    var target;
    var socket;
    
    beforeEach(function () {
      socket = new EventEmitter();
      target = new Penelope(socket);
    });
    
    it('requires an initializer', function() {
      assert.throws(
        function () {
          target.openSubsocket();
        },
        /initializer is required/
      );
    });
    
    it('requires an kind', function() {
      assert.throws(
        function () {
          target.openSubsocket(
            function () {}
          );
        },
        /kind is required/
      );
    });
    
    it('requires params', function() {
      assert.throws(
        function () {
          target.openSubsocket(
            function () {},
            'kind1'
          );
        },
        /params are required/
      );
    });
  });
});