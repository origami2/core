var testData = {
  "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4\nPKtgrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQae\nsmwcu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIi\nGaR/A/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTE\nS+NTkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylh\nX2+xsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQABAoIBAQCzPkfPKGiXj/ho\npviEbfmfR9W2w0F+1V03Wk1Rjo8whxH6Rg9DuJpJcDOza95W2B+uz0Ds64Z6av0j\nmiK8SU4T8S8Yl8merXzHKCdq0IUBzRRan2s/48dsaRhn+G+wSs5yvQWAz0eXHGhR\n2xaPvXSTZdqnPzF/mrgUC3mp538a3TuqNzJYfowY4d+Rn6+8K5EqcWHHBBINBp90\nhWPxGMp2mTFDsycQaQjDsfGJDAlFtQJ0FlrU0KAHt5KhaX7jMvOvD7AuL8aEnnWy\n9yTwb+POpPxrz1a+6p4U7YIPef3yVmWS+QUI2aDtgz7LMlxyTfkX4OiwmBGw51ED\niIwKCYwBAoGBAOfD1WkLhjqAqPDYdm1VkRYQILDWv6vUhznevw/J8yItnpYakRe7\njmhdiQh/p3ieOaA/l31p2ZR4OGT2hZwefAFYbwVoCOAtoIelbJwz5Mu1H7mMBuav\n23/K41edQ25zRbEjy1f20CMBHbEhF+64ufRGUUnJcA0gV9dO4ZvdvQ0jAoGBAMju\nwM/T0sTM83l1IPRvd7moTKzhOt2dJXq660PVY1FqLKSvXicCsFQtEd4J2K2Aik+n\n7A9m0DJx/QauPZ3Dk2K1dWCXqTRp7Yjhvpmw2mvrTfRNFww+Ths15XDCAtRWzUSV\n8yB9TnTH8opeUejWcoI8VikhVtkR8hF5m0xhGXEZAoGAYe4Rwu0nrpimyf3tLI23\nXIc8CPz0yHppGT7RpK5EmfCEzhAztr99kQPU305xSToyR1AXhtqvIVkbGy4/jpQi\n+b6QSsyG05Lz/d0cY3RZ/OlvmktcryUnrnvgTCkbURRMImlphqW3lNLn5OyC3FAZ\n4unDd1YyjplYBJZEJkQvmdkCgYBbtV0bfjq8yC54SRV5e0bR3hbg8QvFCmyIz7eB\nhmuGRUeLAt+ePoPsZ4W4KhZTWk/Ge4YqoWp1G8G4wt4sm48xhlDEfXQlEBPyXdZ4\nn1eR2hwCXQ0f8XAEy1ylUmeoMtYNb8NAGPEuK/RvYw1PkFFDT/ajXywcdyxc/Xv8\nKHVMQQKBgFPnGNRfLtCVfONwcveqfUeR4xS4UJkTNjLExTzBRKoUuKLbZwV5tHEr\nBFoMaM3V5pPLKfImO4BYZeuPdB1DBG3dsxL5G5TzWBauJq7OGAQmXuHecV4pblHm\nPO9yEktLnn8QAkbiePv+PhFFnSAa93cYHNtM68bBTdLrohBzZbDW\n-----END RSA PRIVATE KEY-----",
  "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAteklWX/XJsoFjXHL6AytmDxXRd78dMzcRsDX1Tt/3pkAQlC4PKtg\nrJGfOLQ1i4GckhK15kwQpFnIPX3eWDbYB1rXsqTMNDr4mOI+/zT2bnA7BQaesmwc\nu/FNtHWQhrOgVtEIHD6zh6CZrpIMGC6zccURDQrBEtLONAamplDYLQbalyIiGaR/\nA/Yzc2Rj6OqsCJybFoLhziARseobQnbo3vnRW3VggGbsat6ZpCGV02ed3PTES+NT\nkgJGMU5kDOHQBMWRoS+gFxE2p5GNlSma0vPwY3BqvbVDuQdJwXJ19k0n3ylhX2+x\nsVuCFpHVKt6+dlM3nediTKXDJjXtpa27awIDAQAB\n-----END RSA PUBLIC KEY-----"
};

var utils = require('../crypto-utils');
var assert = require('assert');

describe('CryptoUtils', function () {
  describe('.asPublicKey', function () {
    it('recognizes public key', function () {
      utils.asPublicKey(testData.publicKey);
    });

    it('throws error on invalid public key', function () {
      assert.throws(
        function () {
          utils.asPublicKey('invalid ...');
        },
        /invalid public key/);
    });
  });

  describe('.asPrivateKey', function () {
    it('recognizes private key', function () {
       utils.asPrivateKey(testData.privateKey);
    });

    it('throws error on invalid private key', function () {
      assert.throws(
        function () {
          utils.asPrivateKey('invalid ...');
        },
        /invalid private key/
      );
    });
  });
  
  describe('.asString', function () {
    it('requires key', function () {
      assert.throws(
        function () {
          utils.asString();
        },
        /key is required/
      );
    });
    
    it('returns public key as string', function () {
      var key = utils.asPublicKey(testData.publicKey);
      
      assert.equal(
        testData.publicKey,
        utils.asString(key)
      );
    });
    
    it('returns private key as string', function () {
      var key = utils.asPrivateKey(testData.privateKey);
      
      assert.equal(
        testData.privateKey,
        utils.asString(key, true)
      );
    });
    
    it('returns public key from private key as string', function () {
      var key = utils.asPrivateKey(testData.privateKey);
      
      assert.equal(
        testData.publicKey,
        utils.asString(key)
      );
    });
  });
  
  describe('.randomKey', function () {
    beforeEach(function () {
      this.timeout(10000);
    });
    
    it('generates a random key', function () {
       var key = utils.randomKey();
      
       assert.equal('string', typeof(key));
    
       var asObject = utils.asPrivateKey(key);
       
       assert.equal(512, asObject.getKeySize(), 'default key size is not 512');
    });
    
    it('use specified key size', function () {
       var key = utils.randomKey(128);
      
       assert.equal('string', typeof(key));
    
       utils.asPrivateKey(key);
       
       var asObject = utils.asPrivateKey(key);
       
       assert.equal(128, asObject.getKeySize(), 'key size is not 128');
    });
  });

  var encrypted1;
  var original = {something: true};

  describe('.encrypt', function () {
    it('encrypts data', function () {
      encrypted1 = utils.encrypt(original, testData.publicKey);

      assert.notEqual(
        original,
        encrypted1
      );
    });

    it('requires data', function () {
      assert.throws(
        function () {
          utils.encrypt(null, testData.publicKey);
        },
        /input data is required/
      );
    });

    it('requires a public key', function () {
      assert.throws(
        function () {
          utils.encrypt(original);
        },
        /public key is required/
      );
    });

    it('requires valid public key', function () {
      assert.throws(
        function () {
          utils.encrypt(original, 'invalid...');
        },
        /invalid public key/
      );
    });
  });

  describe('.decrypt', function () {
    it('decrypts data', function () {
      assert.deepEqual(
        utils.decrypt(encrypted1, testData.privateKey),
        original
      );
    });

    it('requires data', function () {
      assert.throws(
        function () {
          utils.decrypt(null, testData.privateKey);
        },
        /input data is required/
      );
    });

    it('requires a private key', function () {
      assert.throws(
        function () {
          utils.decrypt(encrypted1);
        },
        /private key is required/
      );
    });

    it('requires valid private key', function () {
      assert.throws(
        function () {
          utils.decrypt(encrypted1, 'invalid...');
        },
        /invalid private key/
      );
    });
  });

  var signed1;
  describe('.sign', function () {
    it('signs data', function () {
      signed1 = utils.sign(original, testData.privateKey);
    });

    it('requires data', function () {
      assert.throws(
        function () {
          utils.sign(null, testData.privateKey);
        },
        /input data is required/
      );
    });

    it('requires a private key', function () {
      assert.throws(
        function () {
          utils.sign(original);
        },
        /private key is required/
      );
    });

    it('requires valid private key', function () {
      assert.throws(
        function () {
          utils.sign(original, 'invalid...');
        },
        /invalid private key/
      );
    });
  });

  describe('.verify', function () {
    it('verifies signature', function () {
      utils.verify(signed1, testData.publicKey);
    });

    it('requires object data', function () {
      assert.throws(
        function () {
          utils.verify();
        },
        /input data is required/
      );
    });

    it('object data requires .signature', function () {
      assert.throws(
        function () {
          utils.verify({ data: signed1.data }, testData.publicKey);
        },
        /invalid input data/
      );
    });

    it('object data requires .data', function () {
      assert.throws(
        function () {
          utils.verify({ signature: signed1.signature }, testData.publicKey);
        },
        /invalid input data/
      );
    });

    it('requires public key', function () {
      assert.throws(
        function () {
          utils.verify(signed1);
        },
        /public key is required/
      );
    });

    it('fails to verify altered signature', function () {
      var altered = {
        data: signed1.data,
        signature: 'altered'
      };

      assert.equal(
        false,
        utils.verify(altered, testData.publicKey)
      );
    });

    it('fails to verify altered data', function () {
      var altered = {
        data: 'altered',
        signature: signed1.signature
      }

      assert.equal(
        false,
        utils.verify(altered, testData.publicKey)
      );
    });
  });

  var encryptedAndSigned1;

  describe('.encryptAndSign', function () {
    it('returns { data:, signature: }', function () {
      encryptedAndSigned1 = utils.encryptAndSign(original, testData.privateKey, testData.publicKey);

      assert(encryptedAndSigned1);
      assert(encryptedAndSigned1.data);
      assert(encryptedAndSigned1.signature);
    });

    it('requires input data', function () {
      assert.throws(
        function () {
          utils.encryptAndSign(
            null,
            testData.privateKey,
            testData.publicKey
          );
        },
        /input data is required/
      );
    });

    it('requires private key', function () {
      assert.throws(
        function () {
          utils.encryptAndSign(
            original,
            null,
            testData.publicKey
          );
        },
        /private key is required/
      );
    });

    it('requires public key', function () {
      assert.throws(
        function () {
          utils.encryptAndSign(
            original,
            testData.privateKey,
            null
          );
        },
        /public key is required/
      );
    });

    it('result verifies', function () {
      assert.equal(
        true,
        utils.verify(
          encryptedAndSigned1,
          testData.publicKey
        )
      );
    });

    it('result can be decrypted', function () {
      assert.deepEqual(
        original,
        utils.decrypt(encryptedAndSigned1.data, testData.privateKey)
      );
    });
  });

  describe('.decryptAndVerify', function () {
    it('requires input data', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            null,
            testData.privateKey,
            testData.publicKey
          );
        },
        /input data is required/
      );
    });

    it('requires input .data', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            { signature: encryptedAndSigned1.signature },
            testData.privateKey,
            testData.publicKey
          );
        },
        /invalid input data/
      );
    });

    it('requires input .signature', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            { data: encryptedAndSigned1.data },
            testData.privateKey,
            testData.publicKey
          );
        },
        /invalid input data/
      );
    });

    it('requires private key', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            encryptedAndSigned1,
            null,
            testData.publicKey
          );
        },
        /private key is required/
      );
    });

    it('requires public key', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            encryptedAndSigned1,
            testData.privateKey,
            null
          );
        },
        /public key is required/
      );
    });

    it('result verifies', function () {
      assert.deepEqual(
        original,
        utils.decryptAndVerify(
          encryptedAndSigned1,
          testData.privateKey,
          testData.publicKey
        )
      );
    });

    it('fails to decrypt with altered data', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            {
              data: 'altered',
              signature: encryptedAndSigned1.signature
            },
            testData.privateKey,
            testData.publicKey
          );
        },
        /fails to verify/
      );
    });

    it('fails to decrypt with altered signature', function () {
      assert.throws(
        function () {
          utils.decryptAndVerify(
            {
              data: encryptedAndSigned1.data,
              signature: 'altered'
            },
            testData.privateKey,
            testData.publicKey
          );
        },
        /fails to verify/
      );
    });
  });
});