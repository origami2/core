function invoke(fn, self, args) {
  switch (args.length) {
    case 0:
      return fn.call(self);
    case 1:
      return fn.call(self, args[0]);
    case 2:
      return fn.call(self, args[0], args[1]);
    case 3:
      return fn.call(self, args[0], args[1], args[2]);
    case 4:
      return fn.call(self, args[0], args[1], args[2], args[3]);
    default:
      return fn.apply(self, args);
  }
}

function invokeSkipOne(fn, self, args) {
  switch (args.length) {
    case 1:
      return fn.call(self, args[1]);
    case 2:
      return fn.call(self, args[1], args[2]);
    case 3:
      return fn.call(self, args[1], args[2], args[3]);
    case 4:
      return fn.call(self, args[1], args[2], args[3], args[4]);
    default:
      var a = [];
      
      for (var i = 1; i < args.length; i++) {
        a.push(args[i]);
      }
    
      return fn.apply(self, a);
  }
}

module.exports = {
  invoke: invoke,
  invokeSkipOne: invokeSkipOne
};
