FROM node:latest

RUN mkdir -p /src
WORKDIR /src

ADD package.json /src/package.json
RUN npm install && npm install --only=dev

ADD . /src/

RUN npm run test && npm run coverage