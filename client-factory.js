var Client = require('./client');

function ClientFactory() {
}

ClientFactory.prototype.createClient = function (socket, token) {
  if (!socket) throw new Error('socket is required');
  
  return new Promise(function (resolve, reject) {
    socket.emit('describe-apis', function (err, apis) {
      if (err) return reject(err);
      
      resolve(new Client(socket, apis, token));
    });
  });
};

module.exports = ClientFactory;