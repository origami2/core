var fnHelpers = require('./js-function-helpers');
var debug = require('debug')('origami:Plugin');

function plainError(obj) {
  if (obj instanceof Error) {
    return obj.message;
  }
  
  return obj;
}

function Plugin(api) {
  if (typeof(api) !== 'function') throw new Error('API is required to be a function');

  this.api = api;
  this.apiName = api.name;
  this.methods = {};

  for (var methodName in api.prototype) {
    this.methods[methodName] = fnHelpers.getFunctionArgumentNames(api.prototype[methodName]);
  }
}

Plugin.prototype.createInstance = function (context) {
  return new Promise((resolve, reject) => {
    var args = fnHelpers
      .getFunctionArgumentNames(this.api)
      .map((argName) => {
        return context[argName];
      });

    var target;

    try {
      target = Object.create(this.api.prototype);
      
      this.api.apply(target, args);
    } catch (e) {
      debug('%s constructor: error %s', this.apiName, e);
      
      return reject(plainError(e));
    }

    resolve(target);
  });
};

Plugin.prototype.invokeMethod = function (context, methodName, methodParams) {
  return new Promise((resolve, reject) => {
    this
    .createInstance(context)
    .then((instance) => {
      this
      .invokeMethodInInstance(instance, methodName, methodParams)
      .then(resolve)
      .catch(reject);
    })
    .catch((e) => {
      debug('%s.%s: error %s', this.apiName, methodName, e);
      
      reject(plainError(e));
    });
  });
};

Plugin.prototype.invokeMethodInInstance = function (instance, methodName, params) {
  return new Promise((resolve, reject) => {
    var method = this.api.prototype[methodName];

    var args = fnHelpers
      .getFunctionArgumentNames(method)
      .map((argName) => {
        return params[argName];
      });

    try {
      var result = method.apply(instance, args);

      if (result instanceof Promise) {
        result
        .then(resolve)
        .catch(reject);
      } else {
        resolve(result);
      }
    } catch (e) {
      debug('%s.%s: error %s', this.apiName, methodName, e);
      
      reject(plainError(e));
    }
  });
};

Plugin.prototype.describeMethods = function () {
  return this.methods;
};

Plugin.prototype.getName = function () {
  return this.apiName;
};

module.exports = Plugin;
