var jsHelpers = require('./js-function-helpers');

function addApi(socket, token, apiName, methods, api) {
  var ShadowClient = jsHelpers.createApi(
    apiName,
    methods,
    function (apiName, methodName, argNames) {
      return jsHelpers.createMethod(
        methodName,
        argNames,
        function () {
          var argsObject = {};

          for (var i = 0; i < arguments.length; i++) {
            argsObject[argNames[i]] = arguments[i];
          }

          return new Promise((resolve, reject) => {
            var apiToken;
            
            if (typeof(token) === 'function') apiToken = token();
            else apiToken = token;
            
            socket
            .emit(
              'api',
              apiToken,
              apiName,
              methodName,
              argsObject,
              (err, result) => {
                if (err) return reject(err);

                resolve(result);
              }
            );
          });
        }
      );
    }
  );

  api[apiName] = Object.create(ShadowClient.prototype);
  ShadowClient.apply(api[apiName]);
}

function Client (socket, apis, token) {
  if (!socket) throw new Error('socket is required');
  if (!apis) throw new Error('apis are required');

  var api = {};

  for (var apiName in apis) {
    addApi(socket, token, apiName, apis[apiName], api);
  }
  
  socket.on('apis-changed', (newApis) => {
    for (var oldApi in api) delete api[oldApi];
    
    for (var newApi in newApis) addApi(socket, token, newApi, newApis[newApi], api);
  });
  
  return api;
}

module.exports = Client;
