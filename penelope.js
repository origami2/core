var EmittersBus = require('./emitters-bus');
var shortid = require('shortid');
var async = require('async');

function Penelope(mainSocket) {
  if (!mainSocket) throw new Error('socket is required');
  
  this.socket = mainSocket;
  var subsockets = this.subsockets = {};
  var handlers = this.handlers = {};
  
  mainSocket
  .on(
    'open-subsocket',
    function (id, kind, params, callback) {
      var handler = handlers[kind];
      
      if (!handler) return callback('unknown kind');
      
      var bus = new EmittersBus();
    
      var facade = bus.createEmitter();
      var external = bus.createEmitter();
      
      var outgoingQueue = async.queue(
        function (task, callback) {
          var args = task.args;
          var eventName = task.eventName;
          
          var messageCallback = args.slice(-1)[0];
          
          if (typeof(messageCallback) === 'function') {
            args = args.slice(0, -1);
          }
          
          mainSocket.emit.apply(mainSocket, ['subsocket-message', id, eventName, args, messageCallback]);
          
          callback();
        }
      );
      
      outgoingQueue.pause();
      
      external
      .onAny(function (eventName) {
        var args = [];
        
        for (var i = 1; i < arguments.length; i++) {
          args.push(arguments[i]);
        }
        
        outgoingQueue.push({eventName: eventName,args: args});
      });
      
      var incomingQueue = async.queue(function (task, callback) {
        var args = task.args;
        var eventName = task.eventName;
        
        var messageCallback = args.slice(-1)[0];
        
        if (typeof(messageCallback) === 'function') {
          args = args.slice(0, -1);
        }
        
        external.emit.apply(external, [eventName].concat(args).concat([messageCallback]));
          
        callback();
      });
      
      incomingQueue.pause();
      
      var start = function () {
        outgoingQueue.resume();
        incomingQueue.resume();
      };
      
      mainSocket
      .on(
        'disconnect', 
        function () {
          outgoingQueue.pause();
          incomingQueue.pause();
          external.emit('disconnect');
        }
      );
      
      var emitIncoming = function (eventName, message, callback) {
        incomingQueue
        .push({
          eventName: eventName,
          args: message.concat([callback])
        });
      };
      
      subsockets[id] = {
        facade: facade,
        external: external,
        start: start,
        emitIncoming: emitIncoming
      };
      
      try {
        handler(facade, params)
        .then(function () {
          start();
          callback();
        })
        .catch(function (e) {
          callback(e.message || e);
        });
      } catch (e) {
        callback(e.message || e);
      }
    }
  );
  
  mainSocket
  .on(
    'subsocket-message',
    function (id, eventName, message, callback) {
      var subsocket = subsockets[id];
      
      if (!subsocket) return;
      
      subsocket.emitIncoming(eventName, message, callback);
    }
  );
}

Penelope.prototype.addHandler = function (kind, handler) {
  if (!kind) throw new Error('kind is required');
  if (!handler) throw new Error('handler is required');
  
  this.handlers[kind] = handler;
};

Penelope.prototype.openSubsocket = function (initializer, kind, params) {
  if (!initializer) throw new Error('initializer is required');
  if (!kind) throw new Error('kind is required');
  if (!params) throw new Error('params are required');
  
  var mainSocket = this.socket;
  var subsockets = this.subsockets;
  
  return new Promise(function (resolve, reject) {
    var id = shortid.generate();
    var bus = new EmittersBus();
    
    var facade = bus.createEmitter();
    var external = bus.createEmitter();
    
    var outgoingQueue = async.queue(function (task, callback) {
      var args = task.args;
      var eventName = task.eventName;
      
      var messageCallback = args.slice(-1)[0];
      
      if (typeof(messageCallback) === 'function') {
        args = args.slice(0, -1);
      }
      
      mainSocket.emit.apply(mainSocket, ['subsocket-message', id, eventName, args, messageCallback]);
      
      callback();
    });
    
    outgoingQueue.pause();
    
    external.onAny(function (eventName) {
      var args = [];
      
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      
      outgoingQueue.push({eventName: eventName, args: args});
    });
    
    var incomingQueue = async.queue(function (task, callback) {
      var args = task.args;
      var eventName = task.eventName;
      
      var messageCallback = args.slice(-1)[0];
      
      if (typeof(messageCallback) === 'function') {
        args = args.slice(0, -1);
      }
      
      external.emit.apply(external, [eventName].concat(args).concat([messageCallback]));
      
      callback();
    });
    
    incomingQueue.pause();
    
    var start = function () {
      incomingQueue.resume();
      outgoingQueue.resume();
    };
    
    var emitIncoming = function (eventName, message, callback) {
      var task = {
        eventName: eventName,
        args: message.concat([callback])
      };
      
      incomingQueue
      .push(task);
    };
    
    subsockets[id] = {
      facade: facade,
      external: external,
      start: start,
      emitIncoming: emitIncoming
    };
    
    initializer(
      facade, 
      params
    )
    .then(
      function () {
        mainSocket
        .emit(
          'open-subsocket', 
          id,
          kind,
          params,
          function (err) {
            if (err) return reject(err);
            
            start();
            
            resolve();
          }
        );
      }
    );
  });
};

module.exports = Penelope;