var debug = require('debug')('origami:Emitter');

var invoke = require('./utils').invoke;
var invokeSkipOne = require('./utils').invokeSkipOne;

function Emitter(anyEvent, id) {
  this.anyEvent = anyEvent || function () {};
  this.handlers = {
    '': []
  };
  this.id = id;
}

Emitter.prototype.removeListener = function (eventName,listener) {
  if (!this.handlers[eventName]) return;
  
  this.handlers[eventName].splice(this.handlers[eventName].indexOf(listener), 1);
};

Emitter.prototype.removeAllListeners = function (eventName) {
  delete this.handlers[eventName];
};

Emitter.prototype.onAny = function(listener) {
  debug('%s adding listener for any function', this.id);
  
  this.handlers[''].push(listener);
};

Emitter.prototype.offAny = function(listener) {
  debug('%s removing listener for any function', this.id);
  
  var handlers = this.handlers[''];
  
  if (!handlers) return;
  
  for (var i = handlers.length; i >= 0; i--) {
    if (handlers[i] === listener) handlers.splice(i, 1);
  }
};

Emitter.prototype.fireEvent = function (eventName) {
  debug('%s firing event %s', this.id, eventName);
  
  var handlers = this.handlers[eventName] || [];

  for (var i = 0; i < handlers.length; i++) {
    invokeSkipOne(handlers[i], this, arguments);
  }
  
  for (var i = 0; i < this.handlers[''].length; i++) {
    invoke(this.handlers[''][i], this, arguments);
  }
};

Emitter.prototype.on = function (eventName, listener) {
  debug('%s adding listener for %s', this.id, eventName);
  
  (this.handlers[eventName] = this.handlers[eventName] || []).push(listener);
};

Emitter.prototype.addEventListener = Emitter.prototype.on;

Emitter.prototype.off = function (eventName, listener) {
  debug('%s removing listener for %s', this.id, eventName);
  
  var handlers = this.handlers[eventName];
  
  if (!handlers) return;
  
  for (var i = handlers.length; i >= 0; i--) {
    if (handlers[i] === listener) handlers.splice(i, 1);
  }
};

Emitter.prototype.emit = function () {
  debug('%s emitting %s', this.id, arguments[0]);
    
  invoke(this.anyEvent, this, arguments);
};


module.exports = Emitter;