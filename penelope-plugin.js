var Client = require('./client');

function EmitterFunction(token, socket) {
  return function (eventName, message) {
    socket.emit('publish', eventName, token, message);
  };
}

function plainError(obj) {
  if (obj instanceof Error) return obj.message;
  
  return obj;
}

function buildInstanceLocals(stackToken, socket, locals, dependencies, context, knownApis) {
  return new Promise(function (resolve, reject) {
    for (var localName in locals) {
      context[localName] = locals[localName];
    }
    
    context.emit = new EmitterFunction(stackToken, socket);
    
    if (!dependencies || Object.keys(dependencies).length === 0) {
      resolve(context);
    } else {
      var client = new Client(socket, knownApis, stackToken);
          
      for (var depName in dependencies) {
        context[depName] = client[dependencies[depName]];
      }
      
      resolve(context);
    }
  });
}

function PenelopePlugin(plugin, locals, dependencies, events) {
  if (!plugin) throw new Error('plugin is required');
  locals = locals || {};
  var knownApis = {};
  
  return function (socket, params) {
    socket
    .emit('describe-apis', function (err, apis) {
      for (var oldApi in knownApis) delete knownApis[oldApi];
      
      for (var newApi in apis) knownApis[newApi] = apis[newApi];
    });
    
    socket
    .on('apis-changed', function (apis) {
      for (var oldApi in knownApis) delete knownApis[oldApi];
      
      for (var newApi in apis) knownApis[newApi] = apis[newApi];
    });
    
    socket
    .on(
      'describe-methods',
      function (callback) {
        callback(null, plugin.describeMethods());
      }
    );
    
    socket
    .on(
      'event', 
      function (eventName, token, context, message) {
        var handler = events[eventName];
        
        if (!handler) return;
        
        buildInstanceLocals(token, socket, locals, dependencies, context, knownApis)
        .then(function (instanceLocals) {
          plugin
          .createInstance(instanceLocals)
          .then(function (instance) {
            handler.call(instance, eventName, message);
          });
        });
      }
    );
    
    socket
    .on(
      'api',
      function (
        stackToken,
        context,
        methodName,
        methodParams,
        callback
      ) {
        buildInstanceLocals(stackToken, socket, locals, dependencies, context, knownApis)
        .then(function (instanceLocals) {
          plugin
          .invokeMethod(instanceLocals, methodName, methodParams)
          .then(function (result) {
            callback(null, result);
          })
          .catch(function (err) {
            callback(plainError(err));
          });
        })
        .catch(function (err) {
          callback(plainError(err));
        });
      }
    );

    return Promise.resolve();
  };
}

module.exports = PenelopePlugin;