var TokenProviderClient = require('./token-provider-client');

function TokenProviderClientFactory(socket, privateKey, nameRegistry, apis) {
  return function (token, notifications) {
    return new TokenProviderClient(
      socket, 
      apis, 
      token,
      notifications
    );
  };
}

module.exports = TokenProviderClientFactory;